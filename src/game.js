import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'pixi-particles';

import app from './core/app';

import panel from './core/panel';
import specialAttack from "./core/specialAttack";

const isMobile = app.isMobile();

let loaded = PIXI.loader.resources;

let hero;
let supportHero;
let supportHero1;
let supportHero2;
let supportHero3;
let supportHero4;
let enemy;
let stage;

let uiPanel;

let stageArray;
let enemyArray;
let heroAnimationArray = [];

function init() {

    let heroAnimationWeapon1 = [
        loaded.hero_sword_1_0.texture,
        loaded.hero_sword_1_1.texture,
        loaded.hero_sword_1_2.texture,
        loaded.hero_sword_1_3.texture,
        loaded.hero_sword_1_4.texture,
        loaded.hero_sword_1_5.texture,
        loaded.hero_sword_1_6.texture,
        loaded.hero_sword_1_7.texture,
        loaded.hero_sword_1_8.texture,
        loaded.hero_sword_1_9.texture,
    ];

    let heroAnimationWeapon2 = [
        loaded.hero_sword_2_0.texture,
        loaded.hero_sword_2_1.texture,
        loaded.hero_sword_2_2.texture,
        loaded.hero_sword_2_3.texture,
        loaded.hero_sword_2_4.texture,
        loaded.hero_sword_2_5.texture,
        loaded.hero_sword_2_6.texture,
        loaded.hero_sword_2_7.texture,
        loaded.hero_sword_2_8.texture,
        loaded.hero_sword_2_9.texture,
    ];

    let heroAnimationWeapon3 = [
        loaded.hero_sword_3_0.texture,
        loaded.hero_sword_3_1.texture,
        loaded.hero_sword_3_2.texture,
        loaded.hero_sword_3_3.texture,
        loaded.hero_sword_3_4.texture,
        loaded.hero_sword_3_5.texture,
        loaded.hero_sword_3_6.texture,
        loaded.hero_sword_3_7.texture,
        loaded.hero_sword_3_8.texture,
        loaded.hero_sword_3_9.texture,
    ];

    let heroAnimationWeapon4 = [
        loaded.hero_sword_4_0.texture,
        loaded.hero_sword_4_1.texture,
        loaded.hero_sword_4_2.texture,
        loaded.hero_sword_4_3.texture,
        loaded.hero_sword_4_4.texture,
        loaded.hero_sword_4_5.texture,
        loaded.hero_sword_4_6.texture,
        loaded.hero_sword_4_7.texture,
        loaded.hero_sword_4_8.texture,
        loaded.hero_sword_4_9.texture,
    ];

    let heroAnimationWeapon5 = [
        loaded.hero_sword_5_0.texture,
        loaded.hero_sword_5_1.texture,
        loaded.hero_sword_5_2.texture,
        loaded.hero_sword_5_3.texture,
        loaded.hero_sword_5_4.texture,
        loaded.hero_sword_5_5.texture,
        loaded.hero_sword_5_6.texture,
        loaded.hero_sword_5_7.texture,
        loaded.hero_sword_5_8.texture,
        loaded.hero_sword_5_9.texture,
    ];

    let heroAnimationWeapon6 = [
        loaded.hero_sword_6_0.texture,
        loaded.hero_sword_6_1.texture,
        loaded.hero_sword_6_2.texture,
        loaded.hero_sword_6_3.texture,
        loaded.hero_sword_6_4.texture,
        loaded.hero_sword_6_5.texture,
        loaded.hero_sword_6_6.texture,
        loaded.hero_sword_6_7.texture,
        loaded.hero_sword_6_8.texture,
        loaded.hero_sword_6_9.texture,
    ];

    heroAnimationArray.push(heroAnimationWeapon1);
    heroAnimationArray.push(heroAnimationWeapon2);
    heroAnimationArray.push(heroAnimationWeapon3);
    heroAnimationArray.push(heroAnimationWeapon4);
    heroAnimationArray.push(heroAnimationWeapon5);
    heroAnimationArray.push(heroAnimationWeapon6);

    stageArray = [loaded.BG1.texture, loaded.BG2.texture];

    stage = new PIXI.Sprite(stageArray[0]);
    stage.scale.set(720 / 770);
    stage.name = 'stage';

    hero = new PIXI.extras.AnimatedSprite(heroAnimationArray[0]);
    hero.animationSpeed = 0.8;
    hero.loop = false;
    hero.scale.set(720 / 770);
    hero.x = 80;
    hero.y = 530;
    hero.name = 'Hero';

    supportHero = new PIXI.Container();

    supportHero1 = new PIXI.Sprite(loaded.hero_support_1.texture);
    supportHero1.scale.set(720 / 770);
    supportHero1.x = 250;
    supportHero1.y = 580;
    supportHero1.renderable = false;
    supportHero1.name = 'Hero Support 1';

    supportHero2 = new PIXI.Sprite(loaded.hero_support_2.texture);
    supportHero2.scale.set(720 / 770);
    supportHero2.x = 20;
    supportHero2.y = 580;
    supportHero2.renderable = false;
    supportHero2.name = 'Hero Support 2';

    supportHero3 = new PIXI.Sprite(loaded.hero_support_3.texture);
    supportHero3.scale.set(720 / 770);
    supportHero3.x = 340;
    supportHero3.y = 580;
    supportHero3.renderable = false;
    supportHero3.name = 'Hero Support 3';

    supportHero4 = new PIXI.Sprite(loaded.hero_support_4.texture);
    supportHero4.scale.set(720 / 770);
    supportHero4.x = 440;
    supportHero4.y = 580;
    supportHero4.renderable = false;
    supportHero4.name = 'Hero Support 4';

    supportHero.addChild(supportHero1);
    supportHero.addChild(supportHero2);
    supportHero.addChild(supportHero3);
    supportHero.addChild(supportHero4);

    enemyArray = [loaded.enemy_1.texture, loaded.enemy_2.texture, loaded.enemy_1.texture, loaded.enemy_2.texture, loaded.enemy_3.texture];

    enemy = new PIXI.Sprite(enemyArray[0]);
    enemy.scale.set(720 / 770);
    enemy.anchor.set(0.5, 1);
    enemy.x = app.getResolutionX() / 2 + 50;
    enemy.y = 750;
    enemy.name = 'Enemy';

    app.addComponent(stage);
    app.addComponent(enemy);
    app.addComponent(hero);
    app.addComponent(supportHero);

    uiPanel = panel.init();
    specialAttack.init();
}

export function initSound() {

}

function changeHero(index) {
    hero.textures = heroAnimationArray[index];
}

function changeEnemy(index = 0) {
    enemy.texture = enemyArray[index];
}

function changeStage(index = 0) {
    stage.texture = stageArray[index];
}

function tapping() {
    hero.gotoAndPlay(0);
    hitSlashDisplay();
    hitStatusDisplay();
}

setTimeout(() => {
    // setInterval(() => {
    //     tapping()
    // }, 10)
    // setInterval(() => {
    //     specialAttack.init({amount: 5});
    // }, 4000)
}, 1000);

function hitStatusDisplay() {
    let hitBody = [
        loaded.hitEffect_body_shot_1.texture,
        loaded.hitEffect_body_shot_2.texture,
        loaded.hitEffect_body_shot_3.texture,
        loaded.hitEffect_body_shot_4.texture,
        loaded.hitEffect_body_shot_5.texture,
        loaded.hitEffect_body_shot_6.texture,
        loaded.hitEffect_body_shot_7.texture,
        loaded.hitEffect_body_shot_8.texture,
        loaded.hitEffect_body_shot_9.texture,
        loaded.hitEffect_body_shot_10.texture,
        loaded.hitEffect_body_shot_11.texture,
        loaded.hitEffect_body_shot_12.texture,
        loaded.hitEffect_body_shot_13.texture,
        loaded.hitEffect_body_shot_14.texture,
        loaded.hitEffect_body_shot_15.texture,
        loaded.hitEffect_body_shot_16.texture,
        loaded.hitEffect_body_shot_17.texture,
        loaded.hitEffect_body_shot_18.texture,
        loaded.hitEffect_body_shot_19.texture,
    ];

    let hitCritical = [
        loaded.hitEffect_critical_1.texture,
        loaded.hitEffect_critical_2.texture,
        loaded.hitEffect_critical_3.texture,
        loaded.hitEffect_critical_4.texture,
        loaded.hitEffect_critical_5.texture,
        loaded.hitEffect_critical_6.texture,
        loaded.hitEffect_critical_7.texture,
        loaded.hitEffect_critical_8.texture,
        loaded.hitEffect_critical_9.texture,
        loaded.hitEffect_critical_10.texture,
        loaded.hitEffect_critical_11.texture,
        loaded.hitEffect_critical_12.texture,
        loaded.hitEffect_critical_13.texture,
        loaded.hitEffect_critical_14.texture,
        loaded.hitEffect_critical_15.texture,
        loaded.hitEffect_critical_16.texture,
        loaded.hitEffect_critical_17.texture,
        loaded.hitEffect_critical_18.texture,
        loaded.hitEffect_critical_19.texture,
        loaded.hitEffect_critical_20.texture,
        loaded.hitEffect_critical_21.texture,
        loaded.hitEffect_critical_22.texture,
        loaded.hitEffect_critical_23.texture,
        loaded.hitEffect_critical_24.texture,
        loaded.hitEffect_critical_25.texture,
    ];

    let hitHead = [
        loaded.hitEffect_head_shot_1.texture,
        loaded.hitEffect_head_shot_2.texture,
        loaded.hitEffect_head_shot_3.texture,
        loaded.hitEffect_head_shot_4.texture,
        loaded.hitEffect_head_shot_5.texture,
        loaded.hitEffect_head_shot_6.texture,
        loaded.hitEffect_head_shot_7.texture,
        loaded.hitEffect_head_shot_8.texture,
        loaded.hitEffect_head_shot_9.texture,
        loaded.hitEffect_head_shot_10.texture,
        loaded.hitEffect_head_shot_11.texture,
        loaded.hitEffect_head_shot_12.texture,
        loaded.hitEffect_head_shot_13.texture,
        loaded.hitEffect_head_shot_14.texture,
        loaded.hitEffect_head_shot_15.texture,
        loaded.hitEffect_head_shot_16.texture,
        loaded.hitEffect_head_shot_17.texture,
        loaded.hitEffect_head_shot_18.texture,
        loaded.hitEffect_head_shot_19.texture,
        loaded.hitEffect_head_shot_20.texture,
        loaded.hitEffect_head_shot_21.texture,
        loaded.hitEffect_head_shot_22.texture,
        loaded.hitEffect_head_shot_23.texture,
        loaded.hitEffect_head_shot_24.texture,
    ];

    let hitMiss = [
        loaded.hitEffect_miss_1.texture,
        loaded.hitEffect_miss_2.texture,
        loaded.hitEffect_miss_3.texture,
        loaded.hitEffect_miss_4.texture,
        loaded.hitEffect_miss_5.texture,
        loaded.hitEffect_miss_6.texture,
        loaded.hitEffect_miss_7.texture,
        loaded.hitEffect_miss_8.texture,
        loaded.hitEffect_miss_9.texture,
        loaded.hitEffect_miss_10.texture,
        loaded.hitEffect_miss_11.texture,
        loaded.hitEffect_miss_12.texture,
        loaded.hitEffect_miss_13.texture,
        loaded.hitEffect_miss_14.texture,
        loaded.hitEffect_miss_15.texture,
        loaded.hitEffect_miss_16.texture,
        loaded.hitEffect_miss_17.texture,
        loaded.hitEffect_miss_18.texture,
        loaded.hitEffect_miss_19.texture,
        loaded.hitEffect_miss_20.texture,
        loaded.hitEffect_miss_21.texture,
        loaded.hitEffect_miss_22.texture,
        loaded.hitEffect_miss_23.texture,
        loaded.hitEffect_miss_24.texture,
    ];

    let randomHitStatus = [hitMiss, hitBody, hitCritical, hitHead];
    let randomizer = Math.floor(Math.random() * 20);
    let hitrandomPlaceholder;
    if (randomizer < 10) {
        hitrandomPlaceholder = 1;
    } else if (randomizer >= 10 && randomizer < 15) {
        hitrandomPlaceholder = 0;
    }
    else if (randomizer >= 15 && randomizer < 19) {
        hitrandomPlaceholder = 2;
    } else {
        hitrandomPlaceholder = 3;
    }

    panel.updateAttack(hitrandomPlaceholder);

    let hitStatus = new PIXI.extras.AnimatedSprite(randomHitStatus[hitrandomPlaceholder]);
    hitStatus.animationSpeed = 0.8;
    hitStatus.loop = false;
    hitStatus.scale.set(720 / 770);
    hitStatus.x = 100 + Math.random() * 250;
    hitStatus.y = 150 + Math.random() * 350;
    hitStatus.play();
    hitStatus.onComplete = () => {
        hitStatus.destroy();
    };

    app.addComponent(hitStatus);
}

function hitSlashDisplay() {

    let slash1 = [
        loaded.slash_1_0.texture,
        loaded.slash_1_1.texture,
        loaded.slash_1_2.texture,
        loaded.slash_1_3.texture,
        loaded.slash_1_4.texture,
        loaded.slash_1_5.texture,
        loaded.slash_1_6.texture,
        loaded.slash_1_7.texture,
        loaded.slash_1_8.texture,
        loaded.slash_1_9.texture,
        loaded.slash_1_10.texture,
        loaded.slash_1_11.texture,
        loaded.slash_1_12.texture,
        loaded.slash_1_13.texture,
        loaded.slash_1_14.texture,
    ];

    let slash2 = [
        loaded.slash_2_0.texture,
        loaded.slash_2_1.texture,
        loaded.slash_2_2.texture,
        loaded.slash_2_3.texture,
        loaded.slash_2_4.texture,
        loaded.slash_2_5.texture,
        loaded.slash_2_6.texture,
        loaded.slash_2_7.texture,
        loaded.slash_2_8.texture,
        loaded.slash_2_9.texture,
        loaded.slash_2_10.texture,
        loaded.slash_2_11.texture,
        loaded.slash_2_12.texture,
        loaded.slash_2_13.texture,
        loaded.slash_2_14.texture,
    ];

    let randomSlashStatus = [slash1, slash2];

    let randomizer = Math.floor(Math.random() * 2);

    let slashing = new PIXI.extras.AnimatedSprite(randomSlashStatus[randomizer]);
    slashing.animationSpeed = 0.8;
    slashing.loop = false;
    slashing.scale.set(720 / 770);
    slashing.x = (randomizer === 0 ? 200 : 100) + (Math.random() * 100 - 50);
    slashing.y = (randomizer === 0 ? 300 : 400) + (Math.random() * 100 - 50);
    slashing.play();
    slashing.onComplete = () => {
        slashing.destroy();
    };

    app.addComponent(slashing);
}

function supportHeroSelection() {
    return supportHero;
}

function getEnemy() {
    return enemy;
}

export default {
    init,
    initSound,
    changeHero,
    changeEnemy,
    changeStage,
    tapping,
    supportHeroSelection,
    getEnemy,
    hitStatusDisplay
}
