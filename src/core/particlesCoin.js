import * as PIXI from 'pixi.js';
import sound from './sound';
import app, { isMobile } from './app';

// whole particle emitters container
let particlesCoinsContainer;

// individual particle emitter
let bottomCenterEmitterBronze;
let bottomCenterEmitterSilver;
let bottomCenterEmitterGold;

// confetti emitter
let topCenterConfettiEmitterColor1;
let topCenterConfettiEmitterColor2;

function init(options) {
    let newOptions = Object.assign({
        confetti1Color1: 'ffffff', //"439ed7",
        confetti1Color2: 'ffffff', //"002672",
        confetti2Color1: 'ffffff', //"ffe69d",
        confetti2Color2: 'ffffff', //"d6c131",

        coinSpriteList: [
            "a_1.png",
            "a_2.png",
            "a_3.png",
            "a_4.png",
            "a_5.png",
            "a_6.png",
            "a_7.png",
            "a_8.png"
        ],

        confetti1SpriteList : [
            "blue (1).png",
            "blue (2).png",
            "blue (3).png",
            "blue (4).png",
            "blue (5).png",
            "blue (6).png",
            "blue (7).png",
            "blue (8).png",
            "blue (9).png",
            "blue (10).png",
            "blue (11).png",
            "blue (12).png",
        ],

        confetti2SpriteList : [
            "m_gold (1).png",
            "m_gold (2).png",
            "m_gold (3).png",
            "m_gold (4).png",
            "m_gold (5).png",
            "m_gold (6).png",
            "m_gold (7).png",
            "m_gold (8).png",
            "m_gold (9).png",
            "m_gold (10).png",
            "m_gold (11).png",
            "m_gold (12).png",
        ]
    }, options);

    particlesCoinsContainer = new PIXI.Container();
    particlesCoinsContainer.name = 'Particles Coin Container';

    let coinOption = {
        minRot: 240,
        maxRot: 300,
        startSpeed : isMobile() ? 3500 : 2500,
        endSpeed : 100,
        color1 : "ffffff",
        color2 : "ffffff",
        startScale: 0.5,
        endScale: 0.5,
        gravity: isMobile() ? 4100 : 2800
    };

    // bottom Center
    let bottomCenterContainerBronze = new PIXI.particles.ParticleContainer();
    bottomCenterContainerBronze.x = isMobile() ? 320 : 640;
    bottomCenterContainerBronze.y = isMobile() ? 1380 : 820;
    bottomCenterContainerBronze.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerBronze);

    bottomCenterEmitterBronze = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerBronze,

        // The collection of particle images to use
        [
            {
                framerate: 20,
                loop: true,
                textures: [
                    "bronze_1.png",
                    "bronze_2.png",
                    "bronze_3.png",
                    "bronze_4.png",
                    "bronze_5.png",
                    "bronze_6.png"
                ]
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterBronze.particleConstructor = PIXI.particles.AnimatedParticle;

    let bottomCenterContainerSilver = new PIXI.particles.ParticleContainer();
    bottomCenterContainerSilver.x = isMobile() ? 320 : 640;
    bottomCenterContainerSilver.y = isMobile() ? 1380 : 820;
    bottomCenterContainerSilver.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerSilver);

    bottomCenterEmitterSilver = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerSilver,

        // The collection of particle images to use
        [
            {
                framerate: 20,
                loop: true,
                textures: [
                    "silver_1.png",
                    "silver_2.png",
                    "silver_3.png",
                    "silver_4.png",
                    "silver_5.png",
                    "silver_6.png"
                ]
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterSilver.particleConstructor = PIXI.particles.AnimatedParticle;

    let bottomCenterContainerGold = new PIXI.particles.ParticleContainer();
    bottomCenterContainerGold.x = isMobile() ? 320 : 640;
    bottomCenterContainerGold.y = isMobile() ? 1380 : 820;
    bottomCenterContainerGold.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerGold);

    bottomCenterEmitterGold = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerGold,

        // The collection of particle images to use
        [
            {
                framerate: 20,
                loop: true,
                textures: newOptions.coinSpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterGold.particleConstructor = PIXI.particles.AnimatedParticle;


    // Confetti emitter init
    let topCenterContainerColor1 = new PIXI.particles.ParticleContainer();
    topCenterContainerColor1.x = isMobile() ? 320 : 640;
    topCenterContainerColor1.y = -30;
    topCenterContainerColor1.name = 'corner left confetti';
    particlesCoinsContainer.addChild(topCenterContainerColor1);

    topCenterConfettiEmitterColor1 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        topCenterContainerColor1,

        // The collection of particle images to use
        [
            {
                framerate: 30,
                loop: true,
                textures: newOptions.confetti1SpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig({
            minRot: 240,
            maxRot: 300,
            startSpeed : 1500,
            endSpeed : 600,
            color1 : newOptions.confetti1Color1,
            color2 : newOptions.confetti1Color2,
            startScale: 0.25,
            endScale: 0.25,
            gravity: 2200
        })
    );
    topCenterConfettiEmitterColor1.particleConstructor = PIXI.particles.AnimatedParticle;


    let topCenterContainerColor2 = new PIXI.particles.ParticleContainer();
    topCenterContainerColor2.x = isMobile() ? 320 : 640;
    topCenterContainerColor2.y = -30;
    topCenterContainerColor2.name = 'corner left confetti';
    particlesCoinsContainer.addChild(topCenterContainerColor2);

    topCenterConfettiEmitterColor2 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        topCenterContainerColor2,

        // The collection of particle images to use
        [
            {
                framerate: 30,
                loop: true,
                textures: newOptions.confetti2SpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig({
            minRot: 240,
            maxRot: 300,
            startSpeed : 1500,
            endSpeed : 600,
            color1 : newOptions.confetti2Color1,
            color2 : newOptions.confetti2Color2,
            startScale: 0.25,
            endScale: 0.25,
            gravity: 2200
        })
    );
    topCenterConfettiEmitterColor2.particleConstructor = PIXI.particles.AnimatedParticle;

    //add to stage
    app.addComponent(particlesCoinsContainer);

    // Calculate the current time
    let elapsed = Date.now();

// Update function every frame
    let update = function () {

        // Update the next frame
        requestAnimationFrame(update);

        let now = Date.now();

        // The emitter requires the elapsed
        bottomCenterEmitterBronze.update((now - elapsed) * 0.001);
        bottomCenterEmitterSilver.update((now - elapsed) * 0.001);
        bottomCenterEmitterGold.update((now - elapsed) * 0.001);

        topCenterConfettiEmitterColor1.update((now - elapsed) * 0.001);
        topCenterConfettiEmitterColor2.update((now - elapsed) * 0.001);

        elapsed = now;
    };

    // scorner emitting at 1st
    bottomCenterEmitterBronze.emit = false;
    bottomCenterEmitterBronze.autoUpdate = true;
    bottomCenterEmitterSilver.emit = false;
    bottomCenterEmitterSilver.autoUpdate = true;
    bottomCenterEmitterGold.emit = false;
    bottomCenterEmitterGold.autoUpdate = true;

    topCenterConfettiEmitterColor1.emit = false;
    topCenterConfettiEmitterColor1.autoUpdate = true;
    topCenterConfettiEmitterColor2.emit = false;
    topCenterConfettiEmitterColor2.autoUpdate = true;

    // Start the update
    update();
}

function emitCoins(type = '') {
    sound.play('coinSound');
    switch (type) {
        case 'big':
            // bottomCenterEmitterSilver.frequency = 0.1;
            // bottomCenterEmitterSilver.emit = true;
            bottomCenterEmitterGold.frequency = 0.05;
            bottomCenterEmitterGold.emit = true;
            break;
        case 'mega':
            // bottomCenterEmitterBronze.frequency = 0.05;
            // bottomCenterEmitterBronze.emit = true;
            // bottomCenterEmitterSilver.frequency = 0.05;
            // bottomCenterEmitterSilver.emit = true;
            bottomCenterEmitterGold.frequency = 0.02;
            bottomCenterEmitterGold.emit = true;

            topCenterConfettiEmitterColor1.frequency = 0.01;
            topCenterConfettiEmitterColor1.emit = true;
            topCenterConfettiEmitterColor2.frequency = 0.1;
            topCenterConfettiEmitterColor2.emit = true;

            break;
        default:
            throw new Error('invalid particle coins emit type');
    }
}

function stopAllEmit() {
    sound.stop('coinSound');

    bottomCenterEmitterBronze.emit = false;
    bottomCenterEmitterSilver.emit = false;
    bottomCenterEmitterGold.emit = false;

    topCenterConfettiEmitterColor1.emit = false;
    topCenterConfettiEmitterColor2.emit = false;
}

function initConfig(options) {
    let newOptions = Object.assign({
        minRot: 0,
        maxRot: 0,
        startSpeed : 0,
        endSpeed : 0,
        color1 : "ffffff",
        color2 : "ffffff",
        startScale : 0.7,
        endScale : 1.0,
        gravity: 2000
    }, options);
    return {
        scale: {
            list: [
                {
                    value: newOptions.startScale,
                    time: 0
                },
                {
                    value: newOptions.endScale,
                    time: 1
                }
            ],
            isStepped: false,
        },
        color: {
            list: [
                {
                    value: newOptions.color1,
                    time: 0
                },
                {
                    value: newOptions.color2,
                    time: 1
                }
            ],
            isStepped: false
        },
        acceleration: {
            x: 0,
            y: newOptions.gravity
        },
        speed: {
            start: newOptions.startSpeed,
            end: newOptions.endSpeed,
            minimumSpeedMultiplier: 0.5,
            isStepped: false
        },
        startRotation: {
            min: newOptions.minRot,
            max: newOptions.maxRot
        },
        rotationSpeed: {
            min: 0,
            max: 200
        },
        lifetime: {
            min: 2,
            max: 2
        },
        frequency: 0.1,
        emitterLifetime: -1,
        maxParticles: 200,
        pos: {
            x: 0,
            y: 0
        },
        blendMode: 'normal',
        addAtBack: false,
        spawnType: "point",
    }
}

export default {
    init,
    emitCoins,
    stopAllEmit
}