import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import {each} from 'lodash';

import app, {isMobile} from './app';
import ui from './ui';
import panel from './panel';
import game from '../game';
import state from './state';
import specialAttack from './specialAttack';

const loaded = PIXI.loader.resources;

function init(options) {
    state.set('heroSelected', 0);

    // Assign options to replace default options
    const newOptions = Object.assign({
        sliderAlignmentUpAndDown: true,
        spacingOffset: 0,
    }, options);

    // Initialize container to store all paytable assets
    const slider = new PIXI.Container();
    slider.name = 'slider Container';

    // Create a black background the covers the whole screen
    let sliderBackground = new PIXI.Sprite(loaded.pop_up_Menu_title_background.texture);
    // sliderBackground.drawRect(0, 0, 800, 470);
    sliderBackground.name = 'slider Background';
    // sliderBackground.tint = 0x000000;
    sliderBackground.interactive = true;
    slider.addChild(sliderBackground);

    // Element Container Mask
    let elementMask = new PIXI.Graphics().beginFill(0xffffff);
    elementMask.drawRect(0, 0, 800, 470);
    slider.addChild(elementMask);

    // The very start of the element's Y
    let elementContainer = new PIXI.Container();
    elementContainer.x = app.getResolutionX() / 2;
    elementContainer.mask = elementMask;

    let sliderEndAdjustment = 0;

    let nextValue = 0;
    if (newOptions.type === 'hero') {
        let nextElementY = 100 + newOptions.spacingOffset;
        let numberOfHeroSelected = 0;
        each(newOptions.object, (element, index) => {

            let tab;
            let button;
            let currentElementOffset = element.spacingOffsetY ? element.spacingOffsetY : 0;

            tab = new PIXI.Sprite(element.itemTexture);
            tab.anchor.set(0.5);
            tab.scale.set(720 / 800);
            tab.x = 0;
            tab.name = 'hero Tab';

            button = ui.createButton({
                texture: loaded.hero_Selector_Button.texture,
                centerAnchor: true,
                onPointerUp: () => {
                    onOffSupportHero();
                },
                onPointerDown: () => {
                },
                onPointerOver: () => {
                },
                onPointerOut: () => {
                },
            })
            button.scale.set(720 / 770);
            button.x = 220;
            // button.name = panelName[0];

            // use nextElementY to place current element element below previous element
            tab.y = nextElementY;
            button.y = nextElementY - 10;

            // update nextElementY to make sure next element will be placed below current element
            nextElementY = tab.getBounds().bottom + currentElementOffset + tab.height / 2;

            // add current element into container
            elementContainer.addChild(tab);
            elementContainer.addChild(button);

            function onOffSupportHero() {
                let heroRenderable = game.supportHeroSelection().children[index];
                if (heroRenderable.renderable) {
                    heroRenderable.renderable = false;
                    numberOfHeroSelected--;
                } else {
                    heroRenderable.renderable = true;
                    numberOfHeroSelected++;
                }

                state.set('heroSelected', numberOfHeroSelected);
            }
        });
        slider.addChild(elementContainer);
    }
    else if (newOptions.type === 'power') {
        let nextElementX = -350 + newOptions.spacingOffset;
        each(newOptions.object, (element, index) => {

            let button;
            let description;

            description = new PIXI.Sprite(loaded.power_Selector_Description.texture);
            description.scale.set(720 / 800);
            description.y = 250;

            button = ui.createButton({
                texture: element.itemTexture,
                centerAnchor: false,
                onPointerUp: () => {
                    specialAttack.triggerSpecialAttack({power: index, amount: 5});
                },
                onPointerDown: () => {
                },
                onPointerOver: () => {
                },
                onPointerOut: () => {
                },
            })
            button.interactive = true;
            button.scale.set(720 / 770);
            button.y = 20;
            // button.name = panelName[0];
            // use nextElementY to place current element element below previous element
            description.x = nextElementX + 25;
            button.x = nextElementX;
            // update nextElementY to make sure next element will be placed below current element
            nextValue = button.getBounds().right
            nextElementX = nextValue;
            // add current element into container
            elementContainer.addChild(description);
            elementContainer.addChild(button);
        });
        slider.addChild(elementContainer);
    }
    else if (newOptions.type === 'passive') {
        let nextElementX = -350 + newOptions.spacingOffset;
        each(newOptions.object, (element, index) => {

            let tab;
            let currency;
            let description;
            let button;
            let item;

            tab = new PIXI.Sprite(loaded.passive_Selector_Tab.texture);
            tab.anchor.set(0, 0);
            tab.scale.set(720 / 800);
            tab.y = 60;
            tab.name = 'Passive Tab';

            currency = new PIXI.Sprite(loaded.passive_Selector_Currency.texture);
            currency.anchor.set(0, 0);
            currency.scale.set(720 / 800);
            currency.y = 30;
            currency.name = 'Passive Currency';

            description = new PIXI.Sprite(loaded.passive_Selector_Description.texture);
            // description.anchor.set(0.5);
            description.scale.set(720 / 800);
            description.y = 240;
            description.name = 'Passive Description';

            item = new PIXI.Sprite(element.itemTexture);
            // item.anchor.set(0.5);
            item.scale.set(720 / 800);
            item.y = 110;
            item.name = 'Passive Item';

            button = ui.createButton({
                texture: loaded.passive_Selector_Button.texture,
                centerAnchor: false,
                onPointerUp: () => {
                    panel.changeWeapon(index);
                    game.changeHero(index);
                    panel.popUp('Passive Panel');
                },
                onPointerDown: () => {
                },
                onPointerOver: () => {
                },
                onPointerOut: () => {
                },
            })
            button.scale.set(720 / 770);
            button.y = 250;

            // use nextElementY to place current element element below previous element
            // button.x = nextElementX;
            tab.x = nextElementX;
            description.x = nextElementX + 20;
            item.x = nextElementX + 70;
            currency.x = nextElementX - 40;
            button.x = nextElementX + 280;
            // update nextElementY to make sure next element will be placed below current element
            nextValue = tab.getBounds().right + newOptions.spacingOffset;
            nextElementX = nextValue;
            // add current element into container
            elementContainer.addChild(tab);
            elementContainer.addChild(item);
            elementContainer.addChild(currency);
            elementContainer.addChild(description);
            elementContainer.addChild(button);
        });
        slider.addChild(elementContainer);
    }
    else if (newOptions.type === 'special') {
        let nextElementX = -350 + newOptions.spacingOffset;

        let menu;
        let description;
        let buyButton;
        let itemSelected = 0;

        menu = new PIXI.Sprite(loaded.special_menu.texture);
        menu.anchor.set(0, 0);
        menu.scale.set(720 / 800);
        menu.x = 500;
        menu.y = 230;
        menu.name = 'Special Menu';

        description = new PIXI.Sprite(loaded.special_description.texture);
        // description.anchor.set(0.5);
        description.scale.set(720 / 800);
        description.x = 40;
        description.y = 230;
        description.name = 'Special Description';

        description = new PIXI.Sprite(loaded.special_description.texture);
        // description.anchor.set(0.5);
        description.scale.set(720 / 800);
        description.x = 40;
        description.y = 230;
        description.name = 'Special Description';

        buyButton = ui.createButton({
            texture: loaded.special_buy_button.texture,
            centerAnchor: false,
            onPointerUp: () => {
                panel.changeWeapon(index);
            },
            onPointerDown: () => {
            },
            onPointerOver: () => {
            },
            onPointerOut: () => {
            },
        })
        buyButton.scale.set(720 / 770);
        buyButton.x = 519;
        buyButton.y = 290;
        buyButton.name = 'Special Buy';

        each(newOptions.object, (element, index) => {
            let currentElementOffset = element.spacingOffsetX ? element.spacingOffsetX : 0;

            let button;
            let glow;

            const buttonContainer = new PIXI.Container();

            button = ui.createButton({
                texture: element.itemTexture,
                centerAnchor: false,
                onPointerUp: () => {
                    selectTarget();
                    glow.alpha = 1;
                },
                onPointerDown: () => {
                    glow.alpha = 0.5;
                },
                onPointerOver: () => {
                },
                onPointerOut: () => {
                },
            })
            button.selected = false;
            button.scale.set(720 / 770);
            button.y = 40;

            glow = new PIXI.Sprite(loaded.special_item_glow.texture);
            glow.scale.set(720 / 770);
            glow.x = -40;
            glow.y = 0;
            glow.renderable = false;

            buttonContainer.addChild(glow);
            buttonContainer.addChild(button);

            // use nextElementY to place current element element below previous element
            buttonContainer.x = nextElementX;
            // update nextElementY to make sure next element will be placed below current element
            nextValue = button.getBounds().right + newOptions.spacingOffset;
            nextElementX = nextValue;
            elementContainer.addChild(buttonContainer);

            function selectTarget() {
                if (!glow.renderable) {
                    each(elementContainer.children, (otherButton) => {
                        otherButton.texture = newOptions.offTexture;
                        if (otherButton.selected && otherButton !== button) {
                            otherButton.selected = false;
                        }
                        otherButton.children[0].renderable = false;
                    });

                    button.selected = !button.selected;
                    glow.renderable = true;
                }
                else {
                    glow.renderable = false;
                }
            }
        });

        sliderEndAdjustment = -10;

        slider.addChild(menu);
        slider.addChild(description);
        slider.addChild(buyButton);
        slider.addChild(elementContainer);
    }

    //Simple scrolling screen
    sliderBackground.buttonMode = true;
    let initialDistance = 0;
    let isDown = false;
    // if (newOptions.object.length > 1) {
    sliderBackground.on('pointerdown', (event) => {
        // Calculates the initial distance between mouse and center point of element Container
        if (newOptions.sliderAlignmentUpAndDown) {
            const initialPoint = event.data.getLocalPosition(sliderBackground).y;
            initialDistance = initialPoint - elementContainer.y;
        } else {
            const initialPoint = event.data.getLocalPosition(sliderBackground).x;
            initialDistance = initialPoint - elementContainer.x;
        }

        // Start dragging process
        isDown = true;
    });
    sliderBackground.on('pointermove', (event) => {
        if (!isDown) {
            return;
        }


        // Update element container with mouse position with respected distance
        if (newOptions.sliderAlignmentUpAndDown) {
            if (elementContainer.getBounds().bottom <= 1080) {
                TweenMax.to(elementContainer, 0.5, {
                    y: -elementContainer.height + 310,
                });

                isDown = false;
                return;
            }
            else if (elementContainer.getBounds().top >= 790) {
                TweenMax.to(elementContainer, 0.5, {
                    y: 0,
                });

                isDown = false;
                return;
            }
            else {
                let currentPosition = event.data.getLocalPosition(sliderBackground).y;
                elementContainer.y = currentPosition - initialDistance;
            }
        }
        else {
            if (elementContainer.getBounds().right + (newOptions.spacingOffset * 1.5) + sliderEndAdjustment <= (app.getResolutionX() - 25)) {
                TweenMax.to(elementContainer, 0.5, {
                    x: app.getResolutionX() - elementContainer.width + app.getResolutionX() / 2 - 25 - newOptions.spacingOffset * 1.5 + sliderEndAdjustment,
                });
                isDown = false;
                return;
            }
            else if (elementContainer.getBounds().left - newOptions.spacingOffset >= 15) {
                TweenMax.to(elementContainer, 0.5, {
                    x: app.getResolutionX() / 2,
                });
                isDown = false;
                return;
            }
            else {
                let currentPosition = event.data.getLocalPosition(sliderBackground).x;
                elementContainer.x = currentPosition - initialDistance;
            }
        }
    });

    sliderBackground.on('pointerup', () => {
        // Stop dragging process
        isDown = false;
    });
    // }


    app.addComponent(slider);

    return slider;
}

export default {
    init,
}
