import * as PIXI from 'pixi.js';
import { ReelItem } from './ReelItem';

export class AnimatedSpriteItem extends ReelItem {

    constructor(asset: any, symbol: string = null) {
        super(symbol);

        this.item = new PIXI.extras.AnimatedSprite(asset);
        this.item.name = 'item';
        this.item.animationSpeed = 0.1;
        this.item.loop = true;
        this.item.anchor.set(0.5, 0.5);

        this.container.addChild(this.item);
    }

    changeItem(asset: any) {
        this.item['textures'] = asset;
    }

    playAnimation(options: any) {
        (<PIXI.extras.AnimatedSprite> this.item).loop = <boolean> options.loop;

        (<PIXI.extras.AnimatedSprite> this.item).gotoAndPlay(0);
    }

    addAnimation(options: any) {
    }

    playWin(loop = false) {
        let allPromises = [
            this.playAnimation({loop})
        ];

        let winGlowPromise = null;
        if (this.winGlowObject) {
            winGlowPromise = this.winGlowObject.play();
        }
        allPromises.push(winGlowPromise);

        return Promise.all(allPromises);
    }

    playLose(animationName = 'lose', loop = false) {
        this.playAnimation({animationName, loop});
    }

    playIdle(animationName = 'idle', loop = false) {
        this.playAnimation({animationName, loop});
    }

    playSpin(animationName = 'spin', loop = false) {
        this.playAnimation({animationName, loop});
    }

    setAnimationSpeed(value) {
        (<PIXI.extras.AnimatedSprite> this.item).animationSpeed = value;
    };
}