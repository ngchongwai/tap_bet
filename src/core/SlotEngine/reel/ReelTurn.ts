import { TweenMax, Bounce, Back } from 'gsap';
import { each } from 'lodash';

import { Reel } from './Reel';
import utils from '../../utils';

export class ReelTurn extends Reel {

    private itemMapping;
    private promiseForStopping;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelOutIn');
        }

        super(options, totemBank);

        this.itemMapping = [];
    }

    start() {
        each(this.getItems(), (item) => {
            TweenMax.fromTo(item.container, 1, {rotation: 0}, {rotation: 50});
            TweenMax.fromTo(item.container.scale, 1, {x: 1, y: 1}, {x: 0, y: 0, ease: Back.easeIn.config(5)});
        });
    }

    stop(key) {
        each(this.getItems(), (item, index) => {
            if (key) {
                if (typeof key === 'string') {
                    const targetTotem = this.getTotem(key);
                    item.changeItem(targetTotem.asset);

                    // make sure key is an array.
                    key = [key];

                } else {
                    const temp = key[index];
                    const targetTotem = this.getTotem(temp);
                    item.changeItem(targetTotem.asset);
                }
            }

            TweenMax.fromTo(item.container, 0, {rotation: 50}, {rotation: 0, ease: Back.easeOut.config(5)});
            TweenMax.fromTo(item.container.scale, 0, {x: 0, y: 0}, {x: 1, y: 1, ease: Back.easeOut.config(5)});
        });
        return Promise.resolve();
    }
}
