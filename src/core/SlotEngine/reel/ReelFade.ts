import { TweenMax } from 'gsap';
import { each } from 'lodash';

import { Reel } from './Reel';
import sound from '../../sound';
import utils from '../../utils';
import { CoverableItem } from "../reel-item/CoverableItem";

export class ReelFade extends Reel {

    private itemMapping;
    private promiseForStopping;
    private spinningPromises;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelOutIn');
        }

        super(options, totemBank);
    }

    start() {
        each(this.getItems(), (item, index) => {
            setTimeout(() => {
                item.playAnimation({animationName: 'spin_1'});
                item.addAnimation('spin_2');
            }, (this.options.accelerationSpeed < 3 ? 500 : 120) * index);
            //     setTimeout(() => {
            //         item.container.alpha = 1;
            //         item.playAnimation({animationName: 'spin_1'});
            //         TweenMax.to(item.container, 0.5 / this.options.accelerationSpeed, {
            //             alpha: 0,
            //         });
            //     }, this.options.accelerationSpeed < 2 ? (100 * index) : 0);
        });
    }

    stop(key) {

        const allStopPromises = [];

        each(this.getItems(), (item, index) => {

            // add in key that needed for slot to stop
            let targetTotem = null;
            if (key) {
                if (typeof key === 'string') {
                    targetTotem = this.getTotem(key);
                } else {
                    targetTotem = this.getTotem(key[index]);
                }
            }

            allStopPromises.push(new Promise((resolve) => {
                setTimeout(() => {
                    item.container.alpha = 1;
                    item.playAnimation({animationName: 'spin_3'});
                    TweenMax.to(item.container, 0.25, {
                        delay: 0.15,
                        alpha: 0,
                        onComplete: () => {
                            let totem = targetTotem;
                            item.changeItem(totem.asset);
                            item.playAnimation({animationName: 'spin_4'});
                            sound.play('slotSpinningStopSound');
                            sound.stop('slotSpinningSound');
                            item.addAnimation('idle');
                            TweenMax.fromTo(item.container, 0.25, {
                                alpha: 0
                            }, {
                                alpha: 1,
                                onComplete: () => {
                                    resolve();
                                }
                            })
                        }
                    });
                }, (this.options.accelerationSpeed < 3 ? 500 : 120) * index);
            }));

        });

        return Promise.all(allStopPromises);
    }
}
