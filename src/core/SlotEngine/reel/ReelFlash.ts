import { each } from 'lodash';

import { Reel } from './Reel';
import { CoverableItem } from '../reel-item/CoverableItem';
import { SpineItem } from '../reel-item/SpineItem';

export class ReelFlash extends Reel {

    // this is the mapping to indicate whether the item is still flashing or not.
    // it's because we need to support stop randomly.
    isFlashing: Array<boolean>;

    /** ReelFlash options
     * - flashSpeed: fraps per second to change the totem.
     * - flashRandom: stop randomly
     */
    constructor(options: any, totemBank: any) {
        super(options, totemBank);
    }

    start() {

        this.isFlashing = [];

        each(this.getItems(), (item, index) => {

            this.isFlashing[index] = true;

            if (item instanceof CoverableItem) {
                item.cover(this.options.accelerationSpeed);
            }
        });

        this.flashSpin();
    }

    flashSpin() {

        setTimeout(() => {

            let allStopCounter = 0;

            each(this.getItems(), (item, index) => {

                if (!this.isFlashing[index]) {
                    allStopCounter++;
                    return;
                }

                const totem = this.getRandomTotem();
                item.changeItem(totem.asset);
            });

            if (allStopCounter !== this.getItems().length) {
                requestAnimationFrame(() => {
                    this.flashSpin();
                });
            }

        }, 1000 / this.options.flashSpeed);

    }

    stop(key) {

        const allStopPromises = [];

        each(this.getItems(), (item, index) => {

            // add in key that needed for slot to stop
            let targetTotem = null;
            if (key) {
                if (typeof key === 'string') {
                    targetTotem = this.getTotem(key);
                } else {
                    targetTotem = this.getTotem(key[index]);
                }
            }

            // if flashRandom is false, then the timeout should be 0
            // if flashRandom is true, then we should randomize a timeout to make them stop.
            let timeout = this.options.flashRandom ? Math.floor(Math.random() * 200) : 0;

            allStopPromises.push(new Promise((resolve) => {
                setTimeout(() => {
                    this.isFlashing[index] = false;
                    item.changeItem(targetTotem.asset);

                    if (item instanceof CoverableItem) {
                        item.dismissCover();
                    }

                    resolve();
                }, timeout);
            }));

        });

        return Promise.all(allStopPromises);
    }
}
