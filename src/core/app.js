import * as PIXI from 'pixi.js';
import { each, map } from 'lodash';
import 'gsap';
import io from 'socket.io-client';
import environment from 'environment';

import loading from './loading';
import utils from './utils';
import betHistory from './betHistory';
import ui from './ui';
import domUtils from './domUtils';
import browserSupport from './browserSupport';
import session from './session';

domUtils.addStyleString(`
html {
    height: 100%;
}

body {
    margin: 0;
    height: 100%;
    width: 100%;
    display: flex;
    overflow: hidden;
    justify-content: center;
    align-items: center;
    position: fixed;
    background: black;
}
`);

domUtils.addScriptString('', `https://www.googletagmanager.com/gtag/js?id=${environment.googleAnalyticCode}`, true);
domUtils.addScriptString(`
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '${environment.googleAnalyticCode}');
`);

// Does not use Request Animation Frame for tween update
TweenMax.lagSmoothing(0);
TweenMax.ticker.useRAF(false);

let resolutionX = 720;
let resolutionY = 1280;

// A variable to decide whether can init the game
let canInit = true;

/**
 * Load query params
 **/
const isDemo = utils.getQueryParam('isDemo') === 'true';
let lang = utils.getQueryParam('lang') || 'en';

let loader = PIXI.loader;
let loaded = loader.resources;

if (environment.performanceMode) {
    PIXI.settings.RESOLUTION = 1;
} else {
    PIXI.settings.RESOLUTION = 2;
}

// Auth Me
// api.authMe().then((data) => {
//     data.balance = data.balance ? data.balance : 0;
//
//     state.set('balance', data.balance);
//
//     state.set('token', data.token);
//
//     state.set('bonusLevel', data.bonusLevel);
//
//     currency.setCurrency(data.currency);
//
//     // Hack way to filter out free spins and free bet
//     if (data.available_free_spin) {
//         state.set('availableFreeSpin', data.available_free_spin);
//     }
//
//     // We need to check free bet. We don't need to check free bet if there already is free spin
//     if (data.available_free_bet && !state.get('availableFreeSpin')) {
//         state.set('availableFreeBet', data.available_free_bet);
//     }
//
//     if (data.pending_release_free_spin) {
//         state.set('pendingReleaseFreeSpin', data.pending_release_free_spin);
//     } else if (data.pending_release_free_bet) {
//         state.set('pendingReleaseFreeBet', data.pending_release_free_bet);
//     }
//
//     if (panel.walletText) {
//         panel.updateText();
//     }
//
//     // check is data contain bet amount list
//     if (data.bet_amount_list) {
//         let isAllNum = true;
//         let tempArray = map(data.bet_amount_list, (betAmount) => {
//             let temp = parseInt(betAmount);
//
//             // make sure is numeric
//             if (!temp) {
//                 console.log('bet amount list need to be numeric (default bet amount list will be use)');
//                 isAllNum = false;
//             }
//
//             return temp;
//         });
//
//         if (isAllNum) {
//             state.set('betAmountList', tempArray);
//         }
//     }
//
//     state.set('playerDetail', data);
//
//     // init bet history container when everything is loaded
//     const betHistoryParams = data.operator.bet_history_params;
//     betHistory.init(betHistoryParams);
//
//     // update page title
//     const title = lang === 'ch' ? data.game_name_ch : data.game_name;
//     domUtils.updatePageTitle(title);
//
//     // start synchronization
//     sync.start();
//
//     state.set('slotData', data.slots);
//
//     state.set('initData', data);
// });

/**
 * Functions definition
 **/
export function isMobile() {
    return browserSupport.isMobile();
}

export function isDemoMode() {
    return isDemo;
}

export function checkCanInit() {
    return canInit;
}

export function changeFalseCanInit() {
    canInit = false;
}

function setResolutionX(x) {
    resolutionX = x;
}

export function getResolutionX() {
    return resolutionX;
}

function setResolutionY(y) {
    resolutionY = y;
}

export function getResolutionY() {
    return resolutionY;
}

export function getLanguage() {
    return lang;
}

const app = new PIXI.Application({
    width: getResolutionX(),
    height: getResolutionY(),
    transparent: false,
});

document.body.appendChild(app.view);

// attach some useful function into app instance.
app.isMobile = isMobile;
app.isDemoMode = isDemoMode;
app.getResolutionX = getResolutionX;
app.getResolutionY = getResolutionY;
app.getLanguage = getLanguage;

app.getLoader = () => {
    return loader;
};

app.resize = () => {
    let curW = null;
    let curH = null;
    if (isMobile()) {
        curH = window.innerHeight; //! (add - to create an offset in height. Ex: curH = window.innerHeight - 50, creates a 50px offset on height)
        curW = curH / 16 * 9;

        app.renderer.view.style.width = curW + "px";
    }
    else if (!isMobile()) {
        curW = window.innerWidth; //! (add - to create an offset in width. Ex: curW = window.innerWidth - 50, creates a 50px offset on width)
        curH = curW / 16 * 9;

        //! Check if height set by the ratio exceeds the window's height.
        if (curH > window.innerHeight) //! If it exceeds, set height to window's height. - for offset
        {
            app.renderer.view.style.height = (window.innerHeight) + "px";
        }
        else if (curH < window.innerHeight) //! Else set to height set by ratio
        {
            app.renderer.view.style.height = curH + "px";
        }
    }
};

const components = {
    items: [],
    buttons: [],
    text: [],
};

app.getComponents = () => {
    return components;
};

app.addComponent = (component) => {
    components.items.push(component);
    app.stage.addChild(component);
};

app.addButton = (component) => {
    components.buttons.push(component);
    app.stage.addChild(component);
};

app.addText = (component) => {
    components.text.push(component);
    app.stage.addChild(component);
};

app.disableAllButtons = () => {
    each(components.buttons, (buttons) => {
        buttons.interactive = false;
    });
};

app.enableAllButtons = () => {
    each(components.buttons, (buttons) => {
        buttons.interactive = true;
    });
};

loader
    .on('start', function () {
        app.resize();
    })
    .on('progress', function () {
        loading.updateProgress();
    })
    .on('complete', function () {
        console.log('complete');
    });

/**
 * App init last check
 **/
// browser support detection
if (!browserSupport.isBrowserSupported()) {
    canInit = false;
}

// temp: disable temporary
// game instance checking

// session.init();

/**
 * Listeners registrations
 **/

// make sure canvas render when window resize
window.onresize = app.resize;

// When the browser becomes offline, show dialog and force player to refresh the browser
window.addEventListener('offline', () => {
    ui.showDialog(loaded.languageFile.data.disconnect, '', loaded.languageFile.data.reconnect, {
        onPointerUp: () => {
            window.location.reload();
        }
    });
});

// prevent console being open
const debugKeyEnable = environment.debug;
if (!debugKeyEnable) {

    // prevent right click
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    }, false);

    // prevent key combination
    document.addEventListener('keydown', function (e) {
        if (
            (e.ctrlKey && e.shiftKey && e.key === 'i') || // 'i' key
            (e.ctrlKey && e.shiftKey && e.key === 'j') || // 'j' key
            (e.key === 's' && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) || // "S" key + macOS
            (e.ctrlKey && e.key === 'u') || // "U" key
            (event.key === 'F12') // "F12" key
        ) {
            if (e.stopPropagation) {
                e.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
            e.preventDefault();
        }

    }, false);
}

// hide and show the bet history big close button
window.addEventListener('message', (event) => {
    if (event.data === 'HIDE_BET_HISTORY_CLOSE_BUTTON') {
        betHistory.hideCloseButton();
    } else if (event.data === 'SHOW_BET_HISTORY_CLOSE_BUTTON') {
        betHistory.showCloseButton();
    }
});

// connect to socket server to start the session
const token = utils.getQueryParam('token');
io(environment.socketUrl, {
    query: {
        token: token,
        slug: environment.gameName,
    }
});

export default app;
