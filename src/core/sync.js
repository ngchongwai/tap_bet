import api from './api';
import state from './state';
import panel from './panel';

let syncTimeout = null;

function start() {
    syncTimeout = setTimeout(() => {
        api.sync().then((data) => {
            state.set('balance', data.balance);
            try {
                panel.updateText();
            } catch (e) {
                console.log('panel not init');
            }

            start();
        })
    }, 5000);
}

function stop() {
    clearTimeout(syncTimeout);
}

export default {
    start,
    stop,
}
