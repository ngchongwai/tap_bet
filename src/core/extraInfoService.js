import app from './app';
import * as PIXI from 'pixi.js';
import { each } from 'lodash';

let extraInfo;

function init(posX, posY, options = null) {

    let fontOptions = Object.assign({
        fontSize: 15,
        fill: 0xffffff,
        fontWeight: 'bold',
        stroke: "0xffffff",
        strokeThickness: 0
    }, options);

    extraInfo = new PIXI.Text('', fontOptions);
    extraInfo.anchor.set(0, 0.5);
    extraInfo.x = posX;
    extraInfo.y = posY;

    app.addComponent(extraInfo);
}

function updateExtraInfo(codes) {
    if(typeof codes !== "object") {
        throw new Error('Transaction code input need to be an object');
    }

    let temp = '';

    each(codes, (code,index) => {
        temp = temp.concat(`${index}: ` + code + ' ');
    });

    extraInfo.text = temp;
}

export default {
    init,
    updateExtraInfo,
}