import * as PIXI from 'pixi.js';
import app from './app';
import 'gsap';
import { each } from 'lodash';

let backing;

function init(backingColor = 0x000000) {
    backing = new PIXI.Graphics();
    backing.beginFill(backingColor);
    backing.drawRect(0, 0, app.getResolutionX(), app.getResolutionY());
    backing.endFill();
    backing.alpha = 0;
    backing.visible = false;

    app.addComponent(backing);
}

function setAndOpen(frontObjectName = 'Panel Container') {
    if(!backing) {
        throw new Error('Backing not init yet!');
    }

    TweenMax.killTweensOf(backing);
    backing.alpha = 0;

    backing.visible = true;
    let foundFrontObject = false;
    app.stage.removeChild(backing);
    each(app.stage.children, (child, index) => {
        if (child.name === frontObjectName) {
            // console.log('here name: -', child);
            foundFrontObject = true;
            app.stage.addChildAt(backing, index - 1);
        }
    });

    if(!foundFrontObject) {
        console.error('Invalid Layer');

        each(app.stage.children, (child, index) => {
            if (child.name === 'Panel Container') {
                foundFrontObject = true;
                app.stage.addChildAt(backing, index - 1);
            }
        });
    }

    TweenMax.to(backing, 0.5, {alpha: 0.5});
}

function close() {
    if(!backing) {
        throw new Error('Backing not init yet!');
    }

    TweenMax.to(backing, 0.5, {alpha: 0}, {
        onComplete: () => {
            backing.visible = false;
        }
    });
}

export default {
    init,
    setAndOpen,
    close
}