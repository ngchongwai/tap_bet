import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import {each} from 'lodash';

import app, {isMobile} from './app';
import ui from './ui';
import panel from './panel';
import game from '../game';
import state from './state';
import {TweenMax} from 'gsap';
import utils from './utils';

const loaded = PIXI.loader.resources;

let powerGroup = [];

function init() {


    let meteor = [
        loaded.meteor_1.texture,
        loaded.meteor_2.texture,
        loaded.meteor_3.texture,
        loaded.meteor_4.texture,
        loaded.meteor_5.texture,
        loaded.meteor_6.texture,
        loaded.meteor_7.texture,
        loaded.meteor_8.texture,
        loaded.meteor_9.texture,
        loaded.meteor_10.texture,
        loaded.meteor_11.texture
    ]
    let handOfGod = [
        loaded.godHand.texture,
    ]
    let lightning1 = [
        loaded.lightning_1_1.texture,
        loaded.lightning_1_2.texture,
        loaded.lightning_1_3.texture,
        loaded.lightning_1_4.texture,
        loaded.lightning_1_5.texture,
        loaded.lightning_1_6.texture,
        loaded.lightning_1_7.texture,
        loaded.lightning_1_8.texture,
    ]
    let lightning2 = [
        loaded.lightning_2_1.texture,
        loaded.lightning_2_2.texture,
        loaded.lightning_2_3.texture,
        loaded.lightning_2_4.texture,
        loaded.lightning_2_5.texture,
        loaded.lightning_2_6.texture,
        loaded.lightning_2_7.texture,
        loaded.lightning_2_8.texture,
        loaded.lightning_2_9.texture,
        loaded.lightning_2_10.texture,
    ]

    powerGroup = [meteor, handOfGod, lightning1, lightning2];
}

function triggerSpecialAttack(options, frontObjectName = 'Panel Container') {
    const newOptions = Object.assign({
        power: 0,
        amount: 0
    }, options);
    let winEffectContainer = new PIXI.Container;
    winEffectContainer.name = 'Special Attack Container';

    if (newOptions.power === 0) {
        specialAttack1(newOptions, winEffectContainer).then(()=>{
            winEffectContainer.destroy();
        });;
    } else if (newOptions.power === 1) {
        specialAttack2(newOptions, winEffectContainer).then(()=>{
            winEffectContainer.destroy();
        });;
    } else if (newOptions.power === 2) {
        specialAttack3(newOptions, winEffectContainer).then(()=>{
            winEffectContainer.destroy();
        });;
    } else if (newOptions.power === 3) {
        specialAttack4(newOptions, winEffectContainer).then(()=>{
            winEffectContainer.destroy();
        });
    } else {
        console.log('no special attack');
        return;
    }

    if (frontObjectName === '') { // front layer
        app.addComponent(winEffectContainer);
    }
    else {
        each(app.stage.children, (child, index) => {
            if (child.name === frontObjectName) {
                app.stage.addChildAt(winEffectContainer, index - 1);
            }
        });
    }
}

function specialAttack1(newOptions, winEffectContainer) {
    return new Promise((resolve) => {
        for (let i = 0; i < 25; i++) {
            setTimeout(() => {
                game.hitStatusDisplay()
            }, (i * 50 + 800))
        }

        for (let i = 0; i < newOptions.amount; i++) {
            let randomNegative = (Math.round(Math.random() + 1));
            if (randomNegative === 2) randomNegative = -1;
            let xOffset = randomNegative * (Math.random() * app.getResolutionX() / 6);

            // Initialize new winEffect for every interval
            let winEffect = new PIXI.extras.AnimatedSprite(powerGroup[newOptions.power]);
            winEffect.x = xOffset;
            winEffect.y = -xOffset;
            winEffect.animationSpeed = 0.35;
            winEffect.anchor.set(0.5);
            winEffect.rotation = -0.5;
            winEffect.gotoAndPlay(Math.floor(Math.random() * powerGroup[newOptions.power].length));
            const randScale = (Math.random() * 0.5) + 0.5;
            winEffect.scale.set(randScale);
            winEffect.name = 'Power 1';

            // winEffect movement properties
            let winEffectProperties = {
                'xSpeed': (Math.random() * 2) * 3,
                'ySpeed': (Math.random() * 5 + 1),
                'xAcceleration': 0.05,
                'yAcceleration': 0.05,
            };
            winEffectContainer.addChild(winEffect);

            // Needs an item with properties to start a tween. Used tween update to update winEffects
            let dummyContent = {
                'dummyValue': 0,
            };

            TweenMax.to(dummyContent, 3, {
                dummyValue: 10,
                onUpdate: () => {
                    winEffect.x += winEffectProperties.xSpeed;
                    winEffect.y += winEffectProperties.ySpeed;
                    winEffectProperties.xSpeed += winEffectProperties.xAcceleration;
                    winEffectProperties.ySpeed += winEffectProperties.yAcceleration;
                },
                onComplete: () => {
                    winEffect.destroy();
                    resolve();
                }
            });
        }
    })

}

function specialAttack2(newOptions, winEffectContainer) {
    return new Promise((resolve) => {
        for (let i = 0; i < 50; i++) {
            setTimeout(() => {
                game.hitStatusDisplay()
            }, (i * 40 + 1000))
        }

        // Initialize new winEffect for every interval
        let winEffect = new PIXI.extras.AnimatedSprite(powerGroup[newOptions.power]);
        winEffect.animationSpeed = 0.35;
        winEffect.anchor.set(0.5);
        winEffect.rotation = 0.5;
        winEffect.play();

        winEffect.scale.set(5);
        winEffect.name = 'Power 2';

        // winEffect movement properties
        let winEffectProperties = {
            'xSpeed': 3,
            'ySpeed': 4,
            'xAcceleration': 0.05,
            'yAcceleration': 0.05,
        };
        winEffectContainer.addChild(winEffect);

        // Needs an item with properties to start a tween. Used tween update to update winEffects
        let dummyContent = {
            'dummyValue': 0,
        };

        TweenMax.to(dummyContent, 3, {
            dummyValue: 10,
            onUpdate: () => {
                // console.log(game.getEnemy().state, game.getEnemy().state.tracks[0]);
                // console.log(game.getEnemy().state.data.skeletonData.skins[0].attachments[0].root_boundingBox.vertices);
                // console.log(game.getEnemy().state.update(0));
                // if(hitTestRectangle(winEffect, game.getEnemy())) {
                //     console.log(game.getEnemy());
                // }
                winEffect.x += winEffectProperties.xSpeed;
                winEffect.y += winEffectProperties.ySpeed;
            },
            onComplete: () => {
                winEffect.destroy();
                resolve();
            }
        });
    })
}

function specialAttack3(newOptions, winEffectContainer,) {
    return new Promise((resolve) => {
        let totalHit = 0;
        for (let i = 0; i < 100; i++) {
            let randomizer = Math.floor(Math.random() * 20);
            let hitrandomPlaceholder;
            if (randomizer < 10) {
                hitrandomPlaceholder = 1;
            } else if (randomizer >= 10 && randomizer < 15) {
                hitrandomPlaceholder = 0;
            }
            else if (randomizer >= 15 && randomizer < 19) {
                hitrandomPlaceholder = 2;
            } else {
                hitrandomPlaceholder = 3;
            }

            totalHit += hitrandomPlaceholder;

            setTimeout(() => {
                panel.updateAttack(hitrandomPlaceholder, false);
                panel.displayWinAmountWithSprites((hitrandomPlaceholder * (panel.getWeapon() + 1) * (state.get('heroSelected') + 1)), {showAmount: false});
            }, (i * 20 + 200))
        }

        const runningNumberContainerOptions = {scaling: app.isMobile() ? 0.40 : 0.55};
        let runningNumber = {
            val: 0,
            dummyNumber: 0
        };

        let runningNumberContainer = new PIXI.Container();
        runningNumberContainer.name = 'runningNumberContainer';
        TweenMax.to(runningNumber, 2, {
            dummyNumber: 200,
            // Starts bonus running numbers
            onStart: () => {
                runningNumberContainer.y = app.getResolutionY() / 2 + 100;

                TweenMax.to(runningNumber, 1.5, {
                    val: totalHit,
                    ease: Power4.easeInOut,
                    onUpdate: function () {
                        if (runningNumberContainer)
                            app.stage.removeChild(runningNumberContainer);

                        let updatedString = (runningNumber.val);
                        updatedString = parseInt(updatedString).toString();
                        console.log(updatedString);

                        runningNumberContainer = ui.createSpriteNumbers(runningNumberContainerOptions, panel.getSpriteNumberTexture(), updatedString);
                        runningNumberContainer.x = 50+(app.getResolutionX() / 2) - 10 - runningNumberContainer.getBounds().width / 2;
                        runningNumberContainer.y =  + 500;

                        app.stage.addChildAt(runningNumberContainer, utils.getArrayIndexNumByName(app.stage.children, 'Panel Container') - 1);
                    },
                });
            },

            // Updates y value of bonus title and running numbers based on spine.y
            onUpdate: () => {
                if (!runningNumberContainer)
                    return;
            },

            onComplete: function () {
                TweenMax.to(runningNumberContainer, 0.5, {
                    alpha: 0, onComplete: () => {
                        runningNumberContainer.destroy()
                    }
                });
            },
        });

        // Initialize new winEffect for every interval
        let winEffect = new PIXI.extras.AnimatedSprite(powerGroup[newOptions.power]);
        winEffect.x = 400;
        winEffect.y = 500;
        winEffect.animationSpeed = 0.35;
        winEffect.anchor.set(0.5);
        winEffect.play();
        winEffect.scale.set(0);
        winEffect.name = 'winEffects';
        winEffectContainer.addChild(winEffect);

        TweenMax.fromTo(winEffect.scale, 0.5, {
            x: 0,
            y: 0,
        }, {
            x: 5,
            y: 5
        })

        setTimeout(() => {
            TweenMax.fromTo(winEffect.scale, 0.5, {
                x: 5,
                y: 5,
            }, {
                x: 0,
                y: 0,
                onComplete: () => {
                    winEffect.destroy();
                    resolve();
                }
            })
        }, 2000)
    })
}

function specialAttack4(newOptions, winEffectContainer,) {
    return new Promise((resolve) => {
        let totalHit = 0;
        for (let i = 0; i < 100; i++) {
            let randomizer = Math.floor(Math.random() * 20);
            let hitrandomPlaceholder;
            if (randomizer < 10) {
                hitrandomPlaceholder = 1;
            } else if (randomizer >= 10 && randomizer < 15) {
                hitrandomPlaceholder = 0;
            }
            else if (randomizer >= 15 && randomizer < 19) {
                hitrandomPlaceholder = 2;
            } else {
                hitrandomPlaceholder = 3;
            }

            totalHit += hitrandomPlaceholder;

            setTimeout(() => {
                panel.updateAttack(hitrandomPlaceholder, false);
                panel.displayWinAmountWithSprites((hitrandomPlaceholder * (panel.getWeapon() + 1) * (state.get('heroSelected') + 1)), {showAmount: false});
            }, (i * 20 + 200))
        }

        const runningNumberContainerOptions = {scaling: app.isMobile() ? 0.40 : 0.55};
        let runningNumber = {
            val: 0,
            dummyNumber: 0
        };

        let runningNumberContainer = new PIXI.Container();
        runningNumberContainer.name = 'runningNumberContainer';
        TweenMax.to(runningNumber, 2, {
            dummyNumber: 200,
            // Starts bonus running numbers
            onStart: () => {
                runningNumberContainer.y = app.getResolutionY() / 2 + 100;

                TweenMax.to(runningNumber, 1.5, {
                    val: totalHit,
                    ease: Power4.easeInOut,
                    onUpdate: function () {
                        if (runningNumberContainer)
                            app.stage.removeChild(runningNumberContainer);

                        let updatedString = (runningNumber.val);
                        updatedString = parseInt(updatedString).toString();
                        console.log(updatedString);

                        runningNumberContainer = ui.createSpriteNumbers(runningNumberContainerOptions, panel.getSpriteNumberTexture(), updatedString);
                        runningNumberContainer.x = 50+(app.getResolutionX() / 2) - 10 - runningNumberContainer.getBounds().width / 2;
                        runningNumberContainer.y =  + 500;

                        app.stage.addChildAt(runningNumberContainer, utils.getArrayIndexNumByName(app.stage.children, 'Panel Container') - 1);
                    },
                });
            },

            // Updates y value of bonus title and running numbers based on spine.y
            onUpdate: () => {
                if (!runningNumberContainer)
                    return;
            },

            onComplete: function () {
                TweenMax.to(runningNumberContainer, 0.5, {
                    alpha: 0, onComplete: () => {
                        runningNumberContainer.destroy()
                    }
                });
            },
        });

        for (let i = 0; i < newOptions.amount; i++) {
            let randomNegative = (Math.round(Math.random() + 1));
            if (randomNegative === 2) randomNegative = -1;
            let xOffset = randomNegative * (Math.random() * app.getResolutionX() / 6);

            // Initialize new winEffect for every interval
            let winEffect = new PIXI.extras.AnimatedSprite(powerGroup[newOptions.power]);
            winEffect.x = xOffset + app.getResolutionX() * 1 / 3;
            winEffect.y = -100;
            winEffect.animationSpeed = 0.35;
            winEffect.gotoAndPlay(Math.floor(Math.random() * powerGroup[newOptions.power].length));
            winEffect.scale.set(2.5);
            winEffect.name = 'winEffects';

            winEffectContainer.addChild(winEffect);

            setTimeout(() => {
                winEffect.destroy();
                resolve();
            }, 2000)

        }
    })
}

function hitTestRectangle(r1, r2) {

    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {

        //A collision might be occurring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {

            //There's definitely a collision happening
            hit = true;
        } else {

            //There's no collision on the y axis
            hit = false;
        }
    } else {

        //There's no collision on the x axis
        hit = false;
    }

    //`hit` will be either `true` or `false`
    return hit;
};

export default {
    init,
    triggerSpecialAttack,
};