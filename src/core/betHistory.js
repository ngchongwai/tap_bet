import environment from 'environment';

import utils from './utils';
import state from './state';
import domUtils from './domUtils';

function init(queryParams = null) {

    domUtils.addStyleString(`
    
    .bet-history {
        position: absolute;
        top: 100%;
        height: 100%;
        width: 100%;
        transition: all 0.3s;
    }
    
    .bet-history.show {
        top: 0;
    }
    
    .bet-history .btn-close {
        position: absolute;
        top: 0;
        right: 10%;
        font-size: 30px;
        background-color: transparent;
        outline: 0;
        color: #929292;
        border: none;
        cursor: pointer;
        margin: 25px;
        border-radius: 50%;
        width: 60px;
        height: 60px;
        padding: 0;
        background-image: url('./assets/core/button_close.png');
        background-size: cover;
        background-position: center;
    }
    
    .bet-history .btn-close.hide {
        display: none;
    }
    
    @media (max-width: 600px) {
        .bet-history .btn-close {
            top: 0;
            left: 0;
            font-size: 20px;
            width: 40px;
            height: 40px;
            margin: 10px;
        }
    }
    
    .bet-history iframe {
        width: 100%;
        height: 100%;
        border: 0;
    }
    `);

    const betHistoryDiv = document.createElement('div');
    betHistoryDiv.setAttribute('id', 'bet-history');
    betHistoryDiv.classList.add('bet-history');

    // construct url to transaction history
    const transactionHistoryUrl = environment.transactionHistory;
    const gameName = environment.gameName;
    const token = utils.getQueryParam('token');
    const lang = utils.getQueryParam('lang');
    const isDemo = utils.getQueryParam('isDemo');
    const currency = state.get('currency');

    let URL = `${transactionHistoryUrl}?game=${gameName}&token=${token}`;

    if (lang) {
        URL += `&lang=${lang}`;
    }

    if (currency) {
        URL += `&currency=${currency}`;
    }

    if (isDemo) {
        URL += `&isDemo=true`;
    }

    if (queryParams) {
        URL += `&${queryParams}`;
    }

    const iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'bet-history-iframe');
    iframe.setAttribute('src', URL);
    betHistoryDiv.appendChild(iframe);

    const betHistoryCloseButton = document.createElement('button');
    betHistoryCloseButton.setAttribute('id', 'btn-close');
    betHistoryCloseButton.classList.add('btn-close');
    betHistoryCloseButton.addEventListener('click', () => {
        betHistoryDiv.classList.remove('show');

        const betHistoryElement = document.getElementById('bet-history-iframe');
        if (betHistoryElement === null) return;
        const iWindow = betHistoryElement.contentWindow;

        iWindow.postMessage({"for": "closedTransactionDetail"}, transactionHistoryUrl);

    });
    betHistoryDiv.appendChild(betHistoryCloseButton);

    document.body.appendChild(betHistoryDiv);
}

function show() {
    const betHistoryElement = document.getElementById('bet-history');
    betHistoryElement.classList.add('show');
    betHistoryElement.getElementsByTagName('iframe')[0].focus();
}

function hideCloseButton() {
    const closeButton = document.getElementById('btn-close');
    closeButton.classList.add('hide');
}

function showCloseButton() {
    const closeButton = document.getElementById('btn-close');
    closeButton.classList.remove('hide');
}

export default {
    init,
    show,
    hideCloseButton,
    showCloseButton,
}
