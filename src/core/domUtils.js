function addStyleString(str) {
    const node = document.createElement('style');
    node.innerHTML = str;
    document.head.appendChild(node);
}

function addScriptString(str, src = null, isAsync = false) {
    const node = document.createElement('script');
    node.innerHTML = str;

    if (src) {
        node.src = src;
    }

    if (isAsync) {
        node.async = true;
    }

    document.head.appendChild(node);
}

function updatePageTitle(title) {
    document.title = title;
}

export default {
    addStyleString,
    addScriptString,
    updatePageTitle,
}
