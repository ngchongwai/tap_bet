import { isMobile } from './app';
import app from './app';
import * as PIXI from "pixi.js";

let loader = PIXI.loader;
let loadingContainer = new PIXI.Container();
let loadingSprite;
let loadingRect;

function start(options) {
    let rectHeight = 25;
    let rectY = (isMobile()) ? 1000 : (!isMobile()) ? 650 : app.screen.height;

    const newOptions = Object.assign({
        loadingRectColor: 0x27C4EE,
    }, options);

    loadingSprite = new PIXI.Sprite(PIXI.Texture.from('./assets/logo_majagames.png'));
    loadingSprite.anchor.set(0.5);
    loadingSprite.scale.set(0.25);
    loadingSprite.x = app.screen.width / 2;
    loadingSprite.y = app.screen.height / 2;

    loadingRect = new PIXI.Graphics();
    loadingRect.beginFill(newOptions.loadingRectColor);
    loadingRect.drawRect(0, rectY, 1, rectHeight);
    loadingRect.endFill();

    loadingContainer.addChild(loadingSprite);
    loadingContainer.addChild(loadingRect);
    app.stage.addChild(loadingContainer);
}

function updateProgress() {
    loadingRect.scale.x = 12.8 * loader.progress;
}

function destroy(){
    loadingContainer.destroy();
}

export default {
    start,
    updateProgress,
    destroy,
}