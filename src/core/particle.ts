import * as PIXI from 'pixi.js';

import {map, each, find, minBy, sortBy} from 'lodash';
import {TweenMax, Back} from 'gsap';
import app from './app';

export const Spine = PIXI['spine'].Spine;

// Think of this cass as as single particle
// This class should consist the properties of how
// 1 SINGLE particle moves in the canvas.

export default class Particle {
    protected container: PIXI.Container = new PIXI.Container();
    protected options: any;
    protected particle: any;

    /**
     * Represents single particle system
     * @constructor
     */
    constructor(options: any) {
        this.options = Object.assign({
            // Textures
            animatedSprite: null,
            spine: null,

            // Properties
            gravity: 0,

            // todo not necessary or to rename to offset
            position: {
                x: 0,
                y: 0,
            },

            speed: {
                x: 0,
                y: 0,
            },
            minSpeed: {
                x: 0,
                y: 0,
            },
            maxSpeed: {
                x: 0,
                y: 0,
            },

            acceleration: {
                x: 0,
                y: 0,
            },
            minAcceleration: {
                x: 0,
                y: 0,
            },
            maxAcceleration: {
                x: 0,
                y: 0,
            },

            alpha: {
                start: 1,
                end: 1,
            },

            scale: {
                start: 1,
                end: 1,
            },

            lifeSpan: 10, // In Tick duration
        }, options);


        // Checking & ensuring exactly 1 sprite texture or 1 spine data is passed in
        if (!this.options.animatedSprite && !this.options.spine) {
            throw new Error('Particle class requires either a sprite property or spine property');
        }

        if (this.options.animatedSprite && this.options.spine) {
            throw new Error('Particle can only recieve 1 sprite or 1 spine');
        }

        // Create function
        this.create();

        this.container = new PIXI.Container();
        this.container.name = 'Particle Container';
        this.container.addChild(this.particle);
    }

    // Creates a new particle instance, either type of PIXI.Sprite or PIXI.Spine
    create() {
        if (this.options.animatedSprite) {
            this.particle = this.options.animatedSprite;
            delete this.options['spine'];
        }
        else if (this.options.spine) {
            this.particle = this.options.spine;
            delete this.options['sprite'];
        }

        // We should assign the speeds and properties here
        each(this.options, (option, key) => {
            if (typeof option === 'function') {
                this.options[key] = option();
            } else {
                this.options[key] = option;
            }
        });
    }

    update() {
        let elapsedTime = 0;
        let particleTicker = new PIXI.ticker.Ticker();

        this.particle.x = this.options.position.x;
        this.particle.y = this.options.position.y;
        this.particle.alpha = this.options.alpha.start;
        this.particle.scale = this.options.scale.start;

        if (this.options.animatedSprite) {
            this.particle.animationSpeed = this.options.animationSpeed;
            this.particle.play();
        } else if (this.options.spine) {
            this.particle.state.setAnimation(0, this.options.animationName, this.options.loop);
        }

        // This is the ticker for each single particle system
        // We should transform the particle accordingly to the properties
        particleTicker.add((deltaTime) => {
            elapsedTime += deltaTime;
            if (elapsedTime > this.options.lifeSpan) {
                this.container.destroy();
                particleTicker.stop();
                return;
            }

            // Update Position with Speed
            this.particle.x += this.options.speed.x;
            this.particle.y += this.options.speed.y;

            // Leaving this here because we may need it in the future. Will need some polishing, but ready to work once uncommented.
            // Update Speed with Acceleration
            /* this.options.speed.x = (this.options.speed.x > this.options.maxSpeed.x) ?
                this.options.maxSpeed.x :
                this.options.speed.x + this.options.acceleration.x;

            this.options.speed.y = (this.options.speed.y > this.options.maxSpeed.y) ?
                this.options.maxSpeed.y + this.options.gravity :
                this.options.speed.y + this.options.acceleration.y + this.options.gravity; */

            this.options.speed.x += this.options.acceleration.x;
            this.options.speed.y += this.options.acceleration.y + this.options.gravity;

            /**
             ** Formula to calculate properties with start and end properties
             **
             ** (Linear equation)
             ** (((endValue - startValue) / lifeSpan) * elapsedTime) + startValue
             **
             **/

            // Update Alpha over Tick
            this.particle.alpha = (((this.options.alpha.end - this.options.alpha.start) / this.options.lifeSpan) * elapsedTime) + this.options.alpha.start;

            // Update Scale over Tick
            this.particle.scale.set((((this.options.scale.end - this.options.scale.start) / this.options.lifeSpan) * elapsedTime) + this.options.scale.start);
        });

        particleTicker.start();
    }

    getContainer() {
        return this.container;
    }
}

