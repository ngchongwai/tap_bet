import { each } from 'lodash';
import state from './state';

let currencies = {
    USD: '$',
    CNY: '¥',
    JPY: '¥',
    THB: '฿',
    IDR: 'Rp',
    VND: '₫',
};

function setCurrency(data) {
    if (!data) {
        throw new Error('Must pass data');
    }

    each(currencies, (currency, key) => {
        if (data.toUpperCase() === key) {
            state.set('currency', currency);
            return true;
        }
    });

    if (!state.get('currency')) {
        throw new Error('Currency passed in is invalid');
    }
}

function getCurrency() {
    return state.get('currency');
}

export default {
    setCurrency,
    getCurrency,
}