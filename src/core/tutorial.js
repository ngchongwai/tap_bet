import * as PIXI from 'pixi.js';
import 'pixi-spine';
import { TweenMax } from 'gsap';
import { each } from 'lodash';

import app, { isMobile } from './app';
import utils from './utils';
import state from './state';

let loaded = PIXI.loader.resources;

let tutorialContainer;
let skipButton;
let cursor;
let tutorialPromise;
let assetsInit;
let currentPage;

function init(options = null) {
    assetsInit = false;

    // check is player newcomer
    let tutorialToken = state.get('token') + (isMobile() ? 'p' : 'l');

    if (localStorage.getItem(tutorialToken)) {
        // return;
    }

    tutorialContainer = new PIXI.Container();
    let pageContainer = new PIXI.Container();

    const pageOptions = Object.assign({
        tutorialTextures: [],
        cursorRoutine: [
            {'showCursor': false, 'x': 380, 'y': 1080},
            {'showCursor': true, 'x': 380, 'y': 1080},
            {'showCursor': true, 'x': 380, 'y': 770},
            {'showCursor': true, 'x': 600, 'y': 1110},
            {'showCursor': true, 'x': 170, 'y': 1110},
            {'showCursor': true, 'x': 240, 'y': 1230},
            {'showCursor': true, 'x': 530, 'y': 1230},
            {'showCursor': false, 'x': 670, 'y': 1230},
            {'showCursor': true, 'x': 670, 'y': 1230},
            {'showCursor': true, 'x': 90, 'y': 1230},
            {'showCursor': false, 'x': 550, 'y': 680},
            {'showCursor': true, 'x': 550, 'y': 680},
            {'showCursor': true, 'x': 280, 'y': 250},
            {'showCursor': false, 'x': 0, 'y': 0},
            {'showCursor': false, 'x': 0, 'y': 0},
            {'showCursor': false, 'x': 0, 'y': 0},
        ],
        playButtonOptions: {
            width: isMobile() ? 305 : 115,
            height: isMobile() ? 305 : 115,
            x: isMobile() ? app.getResolutionX() / 2 : 1196,
            y: isMobile() ? 720 : 97,
        },
        skipButtonOptions: {
            width: 200,
            height: 100,
            x: 648,
            y: 120,
        },
    }, options);

    //texture checking
    // if (!pageOptions.tutorialTextures[currentIndex] || !pageOptions.tutorialTextures[currentIndex] instanceof PIXI.Texture) {
    //     throw new Error('tutorial page need texture!!');
    // }
    
    if (isMobile()) {

        // define texture
        let pageTextures = [
            loaded.tutorial.texture,
            loaded.tutorial1.texture,
            loaded.tutorial2.texture,
            loaded.tutorial3.texture,
            loaded.tutorial4.texture,
            loaded.tutorial5.texture,
            loaded.tutorial6.texture,
            loaded.tutorial7.texture,
            loaded.tutorial8.texture,
            loaded.tutorial9.texture,
            loaded.tutorial10.texture,
            loaded.tutorial11.texture,
            loaded.tutorial12.texture,
            loaded.tutorial13.texture,
            loaded.tutorial14.texture,
            loaded.tutorial15.texture,
        ];

        let cursorAnimationTex = [
            loaded.cursor1.texture,
            loaded.cursor2.texture,
            loaded.cursor3.texture,
            loaded.cursor4.texture,
        ];


        let playButton;
        currentPage = 0;

        each(pageTextures, (texture, index) => {
            let page = new PIXI.Sprite(texture);
            page.x = texture.width * index;
            page.interactive = true;

            let initialDistance = 0;
            let isDown = false;
            let initialPoint = 0;

            page.on('pointerdown', (event) => {
                // Calculates the initial distance between mouse and center point of element Container
                initialPoint = event.data.getLocalPosition(page).x;
                initialDistance = initialPoint - pageContainer.x;

                // Start dragging process
                isDown = true;
            });

            page.on('pointermove', (event) => {
                if (!isDown || currentPage === pageTextures.length - 1) {
                    return;
                }

                let currentPosition = event.data.getLocalPosition(page).x;

                // Update element container with mouse position with respected distance
                if (pageContainer.getBounds().right <= app.getResolutionX() - 5) {
                    pageContainer.x += 15;
                    isDown = false;
                    return;
                } else if (pageContainer.getBounds().left > 0 || (currentPosition - initialDistance) > 0) {
                    pageContainer.x = 0;
                    isDown = false;
                    return;
                }
                else {
                    pageContainer.x = currentPosition - initialDistance;
                }
            });

            page.on('pointerup', (event) => {
                if(!isDown) {
                    return;
                }

                // Stop dragging process
                isDown = false;

                // calculate distance of start point position and end point position
                let currentPosition = event.data.getLocalPosition(page).x;
                let distance = currentPosition - initialPoint;

                //update current page
                if (distance > 0) { // swipe right
                    if (currentPage - 1 <= 0) {
                        currentPage = 0;
                        pageContainer.x = 0
                        animateCursor(cursor, pageOptions.cursorRoutine, 0);
                    }
                    else {
                        currentPage -= 1;
                        pageContainer.x = -texture.width * currentPage;
                        animateCursor(cursor, pageOptions.cursorRoutine, currentPage);
                    }
                    skipButton.visible = true;
                }
                else if (distance < 0 && currentPage + 1 < pageTextures.length) { // swipe left
                    currentPage += 1;
                    pageContainer.x = -texture.width * currentPage;
                    animateCursor(cursor, pageOptions.cursorRoutine, currentPage);

                    skipButton.visible = !(currentPage === pageTextures.length - 1);
                }
            });

            pageContainer.addChild(page);
        });

        // init Start Button
        let startButton = new PIXI.Sprite(PIXI.Texture.EMPTY);
        startButton.width = 350;
        startButton.height = 90;
        startButton.x = app.getResolutionX() / 2;
        startButton.y = 1130;
        startButton.name = 'play button';
        startButton.interactive = true;
        startButton.buttonMode = true;
        startButton.anchor.set(0.5, 0.5);
        startButton.on('pointerup', () => {
            pageContainer.x = -720;
            animateCursor(cursor, pageOptions.cursorRoutine, 1);
        });
        pageContainer.addChild(startButton);


        // init play button
        playButton = new PIXI.Sprite(PIXI.Texture.EMPTY);
        playButton.width = pageOptions.playButtonOptions.width;
        playButton.height = pageOptions.playButtonOptions.height;
        playButton.x = pageOptions.playButtonOptions.x + (pageTextures[0].width * (pageTextures.length - 1));
        playButton.y = pageOptions.playButtonOptions.y;
        playButton.name = 'play button';
        playButton.interactive = true;
        playButton.buttonMode = true;
        playButton.anchor.set(0.5, 0.5);
        playButton.on('pointerup', () => {
            // exit tutorial
            endTutorial();
        });
        pageContainer.addChild(playButton);

        tutorialContainer.addChild(pageContainer);

        // init skip button
        skipButton = new PIXI.Sprite(loaded.skipButton.texture);
        skipButton.x = pageOptions.skipButtonOptions.x;
        skipButton.y = pageOptions.skipButtonOptions.y;
        skipButton.name = 'skip button';
        skipButton.interactive = true;
        skipButton.buttonMode = true;
        skipButton.anchor.set(0.5, 0.5);
        skipButton.on('pointerup', () => {
            // exit tutorial
            endTutorial();

        });
        tutorialContainer.addChild(skipButton);

        cursor = new PIXI.extras.AnimatedSprite(cursorAnimationTex);
        cursor.loop = true;
        cursor.anchor.set(0.5, 0.5);
        cursor.x = app.getResolutionX() / 2;
        cursor.y = app.getResolutionY() / 2;
        cursor.animationSpeed = 0.1;
        cursor.play();
        cursor.name = 'cursor';
        animateCursor(cursor, pageOptions.cursorRoutine, 0);

        tutorialContainer.addChild(cursor);

    }
    else {
        // init Landscape tutorial
        let currentPage = new PIXI.Sprite(loaded.tutorial.texture);
        currentPage.interactive = true;
        pageContainer.addChild(currentPage);

        let playButton = new PIXI.Sprite(PIXI.Texture.EMPTY);
        playButton.width = pageOptions.playButtonOptions.width;
        playButton.height = pageOptions.playButtonOptions.height;
        playButton.x = pageOptions.playButtonOptions.x;
        playButton.y = pageOptions.playButtonOptions.y;
        playButton.name = 'play button';
        playButton.interactive = true;
        playButton.buttonMode = true;
        playButton.anchor.set(0.5, 0.5);
        playButton.on('pointerup', () => {
            // exit tutorial
            endTutorial();
        });
        pageContainer.addChild(playButton);

        tutorialContainer.addChild(pageContainer);
    }

    assetsInit = true;
    app.addComponent(tutorialContainer);
    tutorialContainer.visible = false;
    tutorialPromise = utils.defer();
}

function show() {
    if(!assetsInit) {
        return new Promise((resolve) => {
            resolve();
        });
    }
    tutorialContainer.visible = true;
    return tutorialPromise.promise;
}

function animateCursor(cursor, cursorRoutine, currentIndex) {
    if (!cursorRoutine[currentIndex].showCursor) {
        TweenMax.killTweensOf(cursor);
        cursor.visible = cursorRoutine[currentIndex].showCursor;
        cursor.x = cursorRoutine[currentIndex].x;
        cursor.y = cursorRoutine[currentIndex].y;
    }
    else {
        cursor.visible = cursorRoutine[currentIndex].showCursor;
        TweenMax.killTweensOf(cursor);
        TweenMax.to(cursor, 0.25, {
            x: cursorRoutine[currentIndex].x, y: cursorRoutine[currentIndex].y,
            onStart: () => {
                cursor.gotoAndStop(0);
            },
            onComplete: () => {
                cursor.gotoAndPlay(0);
            }
        });
    }
}

function endTutorial() {
    tutorialContainer.visible = false;

    // set player tutorial token for one game open once tutorial
    let tutorialToken = state.get('token') + (isMobile() ? 'p' : 'l');
    localStorage.setItem(tutorialToken, '1');

    tutorialPromise.resolve();
}

export default {
    init,
    show,
}