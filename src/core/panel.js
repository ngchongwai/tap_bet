import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import {map, each, sum} from 'lodash';
import environment from 'environment';

import state from './state';

import app, {isMobile, isAndroid} from './app';
import api from './api';
import sound from './sound';
import utils from './utils';
import ui from './ui';
import cheat from './cheat';
import betHistory from './betHistory';
import backing from './backing';
import Emitter from './emitter';
import slider from './slider';

import tutorial from './tutorial';
import session from './session';
import {TweenMax} from "gsap";
import game from "../game";

let loaded = PIXI.loader.resources;

//top panel
let greenLevelGroup = [];
let redLevelGroup = [];
let healthBar;
let enemyHP = 100;
let enemyHPTotal = 100;
let enemyType;
let level;
let stage;
let weaponType;

let spriteNumberTexture;

//bottom panel
let panelContainer;
let lowerButton1;
let lowerButton2;
let lowerButton3;
let lowerButton4;

//mid panel
let statPanel;
let characterWeapon;
let characterWeaponList = [];

//bottom container panel
let popUpMenuStatus = 0;
let heroContainer;
let heroPanel;
let heroTitle;
let heroInfo;
let powerContainer;
let powerPanel;
let powerTitle;
let powerInfo;
let passiveContainer;
let passivePanel;
let passiveTitle;
let passiveInfo;
let specialContainer;
let specialPanel;
let specialTitle;
let specialInfo;

let particles;

let panelName = ['Hero Panel', 'Power Panel', 'Passive Panel', 'Special Panel']

let isExitAutoSpin = false;

const token = utils.getQueryParam('token');

function init(options) {

    enemyType = 0;
    stage = 0;
    weaponType = 0;

    spriteNumberTexture = {
        '0': loaded.sprite_number_0.texture,
        '1': loaded.sprite_number_1.texture,
        '2': loaded.sprite_number_2.texture,
        '3': loaded.sprite_number_3.texture,
        '4': loaded.sprite_number_4.texture,
        '5': loaded.sprite_number_5.texture,
        '6': loaded.sprite_number_6.texture,
        '7': loaded.sprite_number_7.texture,
        '8': loaded.sprite_number_8.texture,
        '9': loaded.sprite_number_9.texture,
    };

    characterWeaponList = [
        loaded.character_weapon_1.texture,
        loaded.character_weapon_2.texture,
        loaded.character_weapon_3.texture,
        loaded.character_weapon_4.texture,
        loaded.character_weapon_5.texture,
        loaded.character_weapon_6.texture
    ];

    particles = [
        loaded.coin00.texture,
        loaded.coin01.texture,
        loaded.coin02.texture,
        loaded.coin03.texture,
        loaded.coin04.texture,
        loaded.coin05.texture,
        loaded.coin06.texture,
        loaded.coin07.texture,
        loaded.coin08.texture,
        loaded.coin09.texture,
        loaded.coin10.texture,
        loaded.coin11.texture,
        loaded.coin12.texture,
        loaded.coin13.texture,
        loaded.coin14.texture
    ]

    panelContainer = new PIXI.Container();
    panelContainer.name = 'Panel Container';

    let redLevelGroup = new PIXI.Sprite(loaded.levelTrackRed.texture);
    redLevelGroup.anchor.set(0.5);
    redLevelGroup.scale.set(720 / 800);
    redLevelGroup.x = app.getResolutionX() / 2;
    redLevelGroup.y = 30;
    redLevelGroup.name = 'Red Level';

    // green Level indicator
    for (let i = 0; i < 4; i++) {
        let green = new PIXI.Sprite(loaded.levelTrackGreen.texture);
        green.anchor.set(0.5);
        green.scale.set(720 / 800);
        green.x = (app.getResolutionX() / 2 - green.width * 2 - 10) + i * (green.width + 5);
        green.y = 28;
        green.renderable = false;
        green.name = `green level ${i}`;
        greenLevelGroup.push(green)
    }

    let healthBarEmpty = new PIXI.Sprite(loaded.hpContainer.texture);
    healthBarEmpty.anchor.set(0.5);
    healthBarEmpty.scale.set(720 / 800);
    healthBarEmpty.x = app.getResolutionX() / 2;
    healthBarEmpty.y = 80;
    healthBarEmpty.name = 'Empty Health Bar';

    let healthBarMasking = new PIXI.Sprite(loaded.hpMask.texture);
    healthBarMasking.anchor.set(0.5);
    healthBarMasking.scale.set(720 / 800);
    healthBarMasking.x = app.getResolutionX() / 2;
    healthBarMasking.y = 80;
    healthBarMasking.name = 'Health Bar Masking';

    healthBar = new PIXI.Sprite(loaded.hpBar.texture);
    healthBar.anchor.set(0.5);
    healthBar.scale.set(720 / 800);
    healthBar.x = app.getResolutionX() / 2;
    healthBar.y = 80;
    healthBar.name = 'Health Bar';
    healthBar.mask = healthBarMasking;

    let tappingButton = ui.createButton({
        texture: PIXI.Texture.EMPTY,
        centerAnchor: false,
        onPointerUp: () => {
            game.tapping();
        },
        onPointerDown: () => {
        },
        onPointerOver: () => {
        },
        onPointerOut: () => {
        },
    })
    tappingButton.width = app.getResolutionX();
    tappingButton.height = 770;
    tappingButton.y = 0;

    //#region Lower Region
    let lowerButtonPanel = new PIXI.Sprite(loaded.lower_button_panel.texture);
    lowerButtonPanel.scale.set(720 / 800);
    lowerButtonPanel.y = app.getResolutionY() - lowerButtonPanel.height;
    lowerButtonPanel.name = 'Lower Button Panel';

    lowerButton1 = ui.createButton({
        texture: loaded.lower_button_1.texture,
        centerAnchor: true,
        onPointerUp: () => {
            popUp(panelName[0]);
        },
        onPointerDown: () => {
        },
        onPointerOver: () => {
        },
        onPointerOut: () => {
        },
    });
    lowerButton1.name = 'Lower Button1';
    lowerButton1.x = 115;
    lowerButton1.y = 83;

    lowerButton2 = ui.createButton({
        texture: loaded.lower_button_2.texture,
        centerAnchor: true,
        onPointerUp: () => {
            popUp(panelName[1]);
        },
        onPointerDown: () => {
        },
        onPointerOver: () => {
        },
        onPointerOut: () => {
        },
    });
    lowerButton2.name = 'Lower Button1';
    lowerButton2.x = 305;
    lowerButton2.y = 83;

    lowerButton3 = ui.createButton({
        texture: loaded.lower_button_3.texture,
        centerAnchor: true,
        onPointerUp: () => {
            popUp(panelName[2]);
        },
        onPointerDown: () => {
        },
        onPointerOver: () => {
        },
        onPointerOut: () => {
        },
    });
    lowerButton3.name = 'Lower Button1';
    lowerButton3.x = 495;
    lowerButton3.y = 83;

    lowerButton4 = ui.createButton({
        texture: loaded.lower_button_4.texture,
        centerAnchor: true,
        onPointerUp: () => {
            popUp(panelName[3]);
        },
        onPointerDown: () => {
        },
        onPointerOver: () => {
        },
        onPointerOut: () => {
        },
    });
    lowerButton4.name = 'Lower Button1';
    lowerButton4.x = 685;
    lowerButton4.y = 83;
    //#endregion

    //#region Center Region
    statPanel = new PIXI.Sprite(loaded.character_panel.texture);
    statPanel.anchor.set(0.5);
    statPanel.scale.set(720 / 800);
    statPanel.x = app.getResolutionX() / 2;
    statPanel.y = 945;
    statPanel.name = 'Stat Panel';

    characterWeapon = new PIXI.Sprite(characterWeaponList[0]);
    characterWeapon.anchor.set(0.5);
    characterWeapon.x = 180;
    characterWeapon.y = 35;
    characterWeapon.name = 'Selected Weapon';
    //#endregion

    //#region Pop Up
    //#region Hero Pop up
    heroContainer = new PIXI.Container();
    heroContainer.y = 1500;
    heroContainer.name = 'Hero Container';

    heroPanel = new PIXI.Sprite(loaded.pop_up_Menu.texture);
    heroPanel.anchor.set(0.5);
    heroPanel.scale.set(720 / 770);
    heroPanel.x = app.getResolutionX() / 2;
    heroPanel.y = 0;
    heroPanel.name = panelName[0];

    heroTitle = new PIXI.Sprite(loaded.pop_up_Menu_title_heroes.texture);
    heroTitle.anchor.set(0.5);
    heroTitle.scale.set(720 / 770);
    heroTitle.x = app.getResolutionX() / 2;
    heroTitle.y = -180;
    heroTitle.name = 'Hero Title';

    let heroList = [
        {
            itemTexture: loaded.hero_Selector_Tab_1.texture
        },
        {
            itemTexture: loaded.hero_Selector_Tab_2.texture
        },
        {
            itemTexture: loaded.hero_Selector_Tab_3.texture
        },
        {
            itemTexture: loaded.hero_Selector_Tab_4.texture
        },
    ];
    heroInfo = slider.init({
        type: 'hero',
        sliderAlignmentUpAndDown: true,
        object: heroList,
    });
    heroInfo.y = -180;
    //#endregion

    //#region Power Pop up
    powerContainer = new PIXI.Container();
    powerContainer.y = 1500;
    powerContainer.name = 'Power Container';

    powerPanel = new PIXI.Sprite(loaded.pop_up_Menu.texture);
    powerPanel.anchor.set(0.5);
    powerPanel.scale.set(720 / 770);
    powerPanel.x = app.getResolutionX() / 2;
    powerPanel.y = 0;
    powerPanel.name = panelName[1];

    powerTitle = new PIXI.Sprite(loaded.pop_up_Menu_title_powers.texture);
    powerTitle.anchor.set(0.5);
    powerTitle.scale.set(720 / 770);
    powerTitle.x = app.getResolutionX() / 2;
    powerTitle.y = -180;
    powerTitle.name = 'Power Title';

    let powerList = [
        {
            itemTexture: loaded.power_Selector_Button_1.texture
        },
        {
            itemTexture: loaded.power_Selector_Button_2.texture
        },
        {
            itemTexture: loaded.power_Selector_Button_3.texture
        },
        {
            itemTexture: loaded.power_Selector_Button_3.texture
        }
    ];
    powerInfo = slider.init({
        type: 'power',
        sliderAlignmentUpAndDown: false,
        object: powerList,
    });
    powerInfo.y = -180;
    //#endregion

    //#region Passive Pop up
    passiveContainer = new PIXI.Container();
    passiveContainer.y = 1500;
    passiveContainer.name = 'Passive Container';

    passivePanel = new PIXI.Sprite(loaded.pop_up_Menu.texture);
    passivePanel.anchor.set(0.5);
    passivePanel.scale.set(720 / 770);
    passivePanel.x = app.getResolutionX() / 2;
    passivePanel.y = 0;
    passivePanel.name = panelName[2];

    passiveTitle = new PIXI.Sprite(loaded.pop_up_Menu_title_passives.texture);
    passiveTitle.anchor.set(0.5);
    passiveTitle.scale.set(720 / 770);
    passiveTitle.x = app.getResolutionX() / 2;
    passiveTitle.y = -180;
    passiveTitle.name = 'Passive Title';

    let passiveList = [
        {
            itemTexture: characterWeaponList[0]
        },
        {
            itemTexture: characterWeaponList[1]
        },
        {
            itemTexture: characterWeaponList[2]
        },
        {
            itemTexture: characterWeaponList[3]
        },
        {
            itemTexture: characterWeaponList[4]
        },
        {
            itemTexture: characterWeaponList[5]
        },
    ];
    passiveInfo = slider.init({
        type: 'passive',
        sliderAlignmentUpAndDown: false,
        object: passiveList,
        spacingOffset: 80,
    });
    passiveInfo.y = -180;

    //#endregion

    //#region Special Pop up
    specialContainer = new PIXI.Container();
    specialContainer.y = 1500;
    specialContainer.name = 'Special Container';

    specialPanel = new PIXI.Sprite(loaded.pop_up_Menu.texture);
    specialPanel.anchor.set(0.5);
    specialPanel.scale.set(720 / 770);
    specialPanel.x = app.getResolutionX() / 2;
    specialPanel.y = 0;
    specialPanel.name = panelName[3];

    specialTitle = new PIXI.Sprite(loaded.pop_up_Menu_title_specials.texture);
    specialTitle.anchor.set(0.5);
    specialTitle.scale.set(720 / 770);
    specialTitle.x = app.getResolutionX() / 2;
    specialTitle.y = -180;
    specialTitle.name = 'Special Title';

    let specialList = [
        {
            itemTexture: loaded.special_item_1.texture
        },
        {
            itemTexture: loaded.special_item_2.texture
        },
        {
            itemTexture: loaded.special_item_3.texture
        },
        {
            itemTexture: loaded.special_item_3.texture
        },
    ];
    specialInfo = slider.init({
        type: 'special',
        sliderAlignmentUpAndDown: false,
        object: specialList,
        spacingOffset: 20,
    });
    specialInfo.y = -180;
    //#endregion
//#endregion

    panelContainer.addChild(redLevelGroup);
    for (let i = 0; i < 4; i++) {
        panelContainer.addChild(greenLevelGroup[i]);
    }

    panelContainer.addChild(healthBarEmpty);
    panelContainer.addChild(healthBarMasking);
    panelContainer.addChild(healthBar);

    panelContainer.addChild(tappingButton);

    panelContainer.addChild(statPanel);
    statPanel.addChild(characterWeapon);

    panelContainer.addChild(heroContainer);
    heroContainer.addChild(heroInfo);
    heroContainer.addChild(heroPanel);
    heroContainer.addChild(heroTitle);

    panelContainer.addChild(powerContainer);
    powerContainer.addChild(powerInfo);
    powerContainer.addChild(powerPanel);
    powerContainer.addChild(powerTitle);

    panelContainer.addChild(passiveContainer);
    passiveContainer.addChild(passiveInfo);
    passiveContainer.addChild(passivePanel);
    passiveContainer.addChild(passiveTitle);

    panelContainer.addChild(specialContainer);
    specialContainer.addChild(specialInfo);
    specialContainer.addChild(specialPanel);
    specialContainer.addChild(specialTitle);

    panelContainer.addChild(lowerButtonPanel);
    lowerButtonPanel.addChild(lowerButton1);
    lowerButtonPanel.addChild(lowerButton2);
    lowerButtonPanel.addChild(lowerButton3);
    lowerButtonPanel.addChild(lowerButton4);
    app.addComponent(panelContainer);

    // const newOptions = Object.assign({
    // }, options);

    // load core sounds
    // sound.addSound('buttonClickSound', loaded.buttonClickSound.sound, {loop: false, volume: 1});
    // sound.addSound('buttonClickReleaseSound', loaded.buttonClickReleaseSound.sound, {loop: false, volume: 1});
    // sound.addSound('100SpinSound', loaded.spin100Sound.sound, {loop: false, volume: 1});

    // ui.bringToFront(panelDisplayContainer);

    game.changeEnemy(enemyType);
    level = 0;
    updateLevel(0);
    changeWeapon(weaponType);
    return panelContainer;
}

function popUp(options = null) {
    let selectedPanel;

    each(panelName, (name, index) => {
        if (name === options) {
            selectedPanel = index + 1;
        }
    });

    let popUpArray = [null, heroContainer, powerContainer, passiveContainer, specialContainer]

    //if pop up not up yet
    if (popUpMenuStatus === 0) {
        popUpMenuStatus = selectedPanel + 1;
        bringCorrectPopUp();
    }
    // if pop up click already open
    else if (selectedPanel === popUpMenuStatus) {
        bringPopDown();
    }
    else {
        return bringPopDown().then(() => {
            bringCorrectPopUp();
        })
    }

    function bringCorrectPopUp() {
        popUpMenuStatus = selectedPanel;
        TweenMax.to(popUpArray[selectedPanel], 0.5, {y: 920})
    }

    function bringPopDown() {
        popUpMenuStatus = 0;
        let checkPromises = map(popUpArray, (popUp) => {
            return new Promise((resolve) => {
                if (popUp) {
                    TweenMax.to(popUp, 0.5, {
                        y: 1500,
                        onComplete: () => {
                            resolve();
                        }
                    })
                } else {
                    resolve();
                }
            });
        });
        return Promise.all(checkPromises)
    }
}

function changeWeapon(weaponNo) {
    characterWeapon.texture = characterWeaponList[weaponNo];
    weaponType = weaponNo;
}

function updateAttack(damageByHit = null, showDamage = true, currentHp = null, totalHp = null) {
    if (currentHp) {
        enemyHP = currentHp;
    }
    if (damageByHit) {
        enemyHP -= (damageByHit * (weaponType + 1) * (state.get('heroSelected') + 1));
    }
    if (totalHp) {
        enemyHPTotal = totalHp;
    }

    if (damageByHit > 0 && showDamage) {
        displayWinAmountWithSprites(damageByHit * (weaponType + 1) * (state.get('heroSelected') + 1));
    }

    if (enemyHP > 0) {
        updateHealthBar((enemyHPTotal - enemyHP) / enemyHPTotal);
    } else {
        enemyType++;
        level++;
        if (enemyType > 4) {
            enemyType = 0;
            level = 0;
            stage++;

            updateStage(stage);
        }
        game.changeEnemy(enemyType)
        enemyHP = (enemyType + 1) * 100;
        enemyHPTotal = (enemyType + 1) * 100;
        updateHealthBar();
        updateLevel(level);
    }
}

function updateHealthBar(healthPercentageBy1 = null) {
    if (healthPercentageBy1) {
        healthBar.x = app.getResolutionX() / 2 + healthPercentageBy1 * healthBar.width;
    } else {
        healthBar.x = app.getResolutionX() / 2;
    }
}

function updateLevel(level) {
    for (let i = 0; i < 5; i++) {
        if (level === i) {
            each(greenLevelGroup, (green, index) => {
                if (index < i) {
                    green.renderable = true;
                } else {
                    green.renderable = false;
                }
            })
        }
    }
}

function updateStage(area) {
    if (area > 1) {
        stage = 0;
    }
    game.changeStage(stage);
}

function displayWinAmountWithSprites(amount, options, coin = null, FrontObjectName = 'Panel Container') {

    let newOptions = Object.assign({
        xPosition: app.getResolutionX() / 2,
        yPosition: app.getResolutionY() / 3,
        tweenToY: -250,
        scaling: 1,
        offset: 250,
        time: 1,
        tweenAlpha: 2,
        showAmount: true,
    }, options);
    // A container to wrap all smaller containers (numbers and particles)
    // Set properties of this container later, once all child have initiated
    let winEffectContainer = new PIXI.Container;
    let numberScaling = amount / (weaponType + 1);
    let numberToScale = 0;
    if (numberScaling === 1) {
        numberToScale = 0.5;
    } else if (numberScaling === 2) {
        numberToScale = 0.75;
    }
    else if (numberScaling === 3) {
        numberToScale = 1;
    }

    let numberContainer = ui.createSpriteNumbers({
        scaling: numberToScale * newOptions.scaling,
        offset: newOptions.offset * newOptions.scaling,
    }, spriteNumberTexture, amount.toString());

    TweenMax.to(numberContainer, newOptions.time, {
        y: newOptions.tweenToY,
        onComplete: function () {
            winEffectContainer.destroy();
        }
    });

    TweenMax.fromTo(numberContainer, newOptions.time / 5 * 3, {
        alpha: 0,
    }, {
        alpha: 1,
    });
    setTimeout(() => {
        TweenMax.to(winEffectContainer, newOptions.time / 5 * 2, {
            alpha: 0,
        });
    }, (1000 * newOptions.time * 3 / 5));

    numberContainer.name = 'Textured Number Container';

    winEffectContainer.x = newOptions.xPosition - numberContainer.getBounds().width / 2 - 100 + (Math.random() * 200);
    winEffectContainer.y = newOptions.yPosition - numberContainer.getBounds().height / 2;

    // Smaller particle container, child of winEffectContainer
    let particleContainer = new PIXI.Container;
    particleContainer.x = newOptions.showAmount ? numberContainer.getBounds().width / 2 : newOptions.xPosition;
    particleContainer.x = numberContainer.getBounds().width / 2;
    particleContainer.y = numberContainer.getBounds().height / 2;
    particleContainer.name = 'Particle Container';

    for (let i = 0; i < amount; i++) {
        let randomNegative = (Math.round(Math.random() + 1));
        if (randomNegative === 2) randomNegative = -1;

        let xOffset = newOptions.showAmount ? (randomNegative * (Math.random() * numberContainer.getBounds().width / 2)) : 0;

        // Initialize new particle for every interval
        let particle = new PIXI.extras.AnimatedSprite(coin ? coin : particles);
        particle.x = xOffset;
        particle.y = numberContainer.y;
        particle.animationSpeed = 0.35;
        particle.anchor.set(0.5);
        particle.play();
        const randScale = (Math.random() * 0.3) + 0.3;
        particle.scale.set(randScale);
        particle.name = 'Particles';

        // Particle movement properties
        let xValues = (Math.random() * 2 - 1) * 6;
        let particleProperties = {
            'xSpeed': xValues,
            'ySpeed': -(Math.random() * 5 + 3),
            'xAcceleration': xValues < 0 ? -0.05 : 0.05,
            'yAcceleration': 0.05,
            'gravity': 0.03,
        };
        particleContainer.addChild(particle);

        // Needs an item with properties to start a tween. Used tween update to update particles
        let dummyContent = {
            'dummyValue': 0,
        };
        TweenMax.to(dummyContent, 3, {
            dummyValue: 10,
            onUpdate: () => {
                particle.x += particleProperties.xSpeed;
                particle.y += particleProperties.ySpeed;
                particle.rotation += 0.15;
                particleProperties.xSpeed += particleProperties.xAcceleration;
                particleProperties.ySpeed += particleProperties.yAcceleration;
                particleProperties.yAcceleration += particleProperties.gravity;
            },
            onComplete: () => {
                particle.destroy();
                winEffectContainer.destroy();
            }
        });
    }

    winEffectContainer.addChild(particleContainer);
    if (newOptions.showAmount) {
        winEffectContainer.addChild(numberContainer);
    }

    if (FrontObjectName === '') { // front layer
        app.addComponent(winEffectContainer);
    }
    else {
        each(app.stage.children, (child, index) => {
            if (child.name === FrontObjectName) {
                app.stage.addChildAt(winEffectContainer, index - 1);
            }
        });
    }
}

function getWeapon() {
    return weaponType;
}

function getSpriteNumberTexture() {
    return spriteNumberTexture;
}

// function runPreEvents(events = []) {
//
//     if (!Array.isArray(events)) {
//         throw new Error('Invalid events list');
//     }
//
//     const preEvents = events.concat();
//     utils.serial(preEvents, runPromise);
// }
//
// function runPromise(task, previous) {
//     return task();
// }

// function autoSpinFn() {
//     session.renewSession();
//     canWinAnimRun = false;
//
//     // stop auto spin if flag is off
//     if (!autoSpinRunning) {
//         spinButton.texture = loaded.spinButton.texture;
//         autoSpinCountdown.alpha = 0;
//         enablePanelButton();
//         return;
//     } else if (autoSpinCriteria.numbered > 0) {
//         if (autoSpinCountdownInt > 0) {
//             autoSpinCountdownInt--;
//             autoSpinCountdown.text = autoSpinCountdownInt;
//         }
//         spinButton.texture = loaded.spinDownButton.texture;
//     }
//
//     // console.log( autoSpinCriteria.numbered === 100 && autoSpinState.spinCount === 0);
//     (autoSpinCriteria.numbered === 100 && autoSpinState.spinCount === 0) ? autoSpinCountdown.scale.set(0.8) : autoSpinCountdown.scale.set(1);
//
//     // check stop criteria
//     if (autoSpinCriteria.winningConditions === 'anyWin' && autoSpinState.winCount > 0) {
//         throw 'win any game';
//     } else if (autoSpinCriteria.winningConditions === 'bonusGame' && autoSpinState.isBonus) {
//         throw 'win bonus game';
//     } else if (autoSpinCriteria.singleWin > 0 && autoSpinState.lastPayout > autoSpinCriteria.singleWin) {
//         throw `single win exceed ${autoSpinCriteria.singleWin}`;
//     } else if (autoSpinCriteria.cashIncrease > 0 && state.get('balance') - autoSpinState.initialBalance >= autoSpinCriteria.cashIncrease) {
//         throw `cash increased`;
//     } else if (autoSpinCriteria.cashDecrease > 0 && -(state.get('balance') - autoSpinState.initialBalance) >= autoSpinCriteria.cashDecrease) {
//         throw `cash decreased`;
//     } else if (autoSpinCriteria.numbered > 0 && autoSpinState.spinCount >= autoSpinCriteria.numbered) {
//         throw 'number of spins';
//     }
//
//     const newBalance = state.get('balance') - utils.getTotalBet();
//
//     // terminate if newBalance less than 0;
//     if (newBalance < 0) {
//         // ui.flash(walletText, '0xff0000', 5);
//         enablePanelButton();
//         ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
//
//         throw 'insufficient balance';
//     }
//
//     state.set('balance', newBalance);
//     updateText();
//
//     // if the stop criteria not meet, continue the spin
//     // note: spinFn should always return a payout of the round.
//     autoSpinState.spinCount = autoSpinState.spinCount + 1;
//     return spinFn(true).then((checkWinObj) => {
//
//         // checkWinObj can be a number of payout, or an object contain payout, isBonus and bonusValue
//         let payout = undefined;
//         let isBonus = false;
//         let bonusValue = undefined;
//         if (Object.prototype.toString.call(checkWinObj) === '[object Object]') {
//             if (typeof checkWinObj.payout === 'undefined') throw new Error('checkWinObj does not contain payout');
//             payout = checkWinObj.payout;
//             isBonus = checkWinObj.isBonus;
//             bonusValue = checkWinObj.bonusValue;
//         } else {
//             payout = checkWinObj;
//             isBonus = false;
//             bonusValue = undefined;
//         }
//
//         payout = payout ? payout : 0;
//         // set last payout to check whether single win exceed or not.
//         autoSpinState.lastPayout = payout;
//
//         // update total win
//         autoSpinState.totalWin = autoSpinState.totalWin + payout;
//
//         // increase win count if payout more than 0
//         autoSpinState.winCount = payout > 0 ? autoSpinState.winCount + 1 : autoSpinState.winCount;
//
//         // is bonus game trigger
//         autoSpinState.isBonus = isBonus;
//         autoSpinState.bonusValue = bonusValue;
//
//         return autoSpinFn();
//     });
// }
//
// function changeItem(item, max) {
//
//     if (item > max) {
//         item = 0;
//     }
//
//     if (item < 0) {
//         item = max;
//     }
//
//     return item;
// }
//
// function changeAutoSpinButtonTexture(autoSpinActive) {
//     if (isMobile()) {
//         autoSpinButton.texture = loaded.autoSpinButton.texture;
//     }
//     else {
//         autoSpinButton.texture = autoSpinActive ? loaded.autoSpinActive.texture : loaded.autoSpinButton.texture;
//     }
// }
//
// function checkIfBettingMax(newOptions) {
//     let betLineLimit = newOptions.betLineOptions[newOptions.betLineOptions.length - 1];
//     let betAmountLimit = betAmountList[betAmountList.length - 1];
//     return (state.get('betLine') >= betLineLimit && state.get('betAmount') >= betAmountLimit) ? true : false;
// }

//! Used to update display text values (line bet, total bet, wallet). Reminder: Keep the users to date!!
// function updateText() {
//     setWalletInDisplay(state.get('balance'));
//     totalBetText.text = state.get('currency') + ' ' + utils.showAsCurrency(utils.getTotalBet());
//     betAmountText.text = state.get('betAmount');
//     if (!fixMultiBetLine) {
//         betLineText.text = state.get('betLine');
//     }
//
//     if (isMobile()) {
//         each(panelDisplayContainer.children, (element) => {
//             if (element.children[0].text === loaded.languageFile.data.balanceC) {
//                 element.children[1].x = element.children[0].x + element.children[0].getBounds().width + 10;
//             }
//             if (element.children[0].text === loaded.languageFile.data.winC) {
//                 element.children[1].x = element.children[0].x + element.children[0].getBounds().width + 10;
//             }
//             if (element.children[0].text === loaded.languageFile.data.winning_bonusC) {
//                 element.children[1].x = element.children[0].x + element.children[0].getBounds().width + 10;
//             }
//         });
//
//         totalBetLabel.x = totalBetText.x - (totalBetText.getBounds().width) - 5;
//     }
// }

// function spinButtonClicked() {
//     // terminate if the previous game is still running.
//     if (isGameRunning) return;
//     if (autoSpinRunning) return;
//     spinIsPressed = true;
//
//     canWinAnimRun = false;
//
//     //reset wallet text color
//     // walletText.style.fill = 0xFFFFFF;
//
//     // Play button effect
//     spinButtonEffect.state.setAnimation(0, 'click', false);
//
//     // We should not deduct the balance if the user has freeSpins or freeBet that they can use.
//     const newBalance =  state.get('balance') - utils.getTotalBet();
//
//     // terminate if newBalance less than 0;
//     if (newBalance < 0) {
//         // ui.flash(walletText, '0xff0000', 5);
//         ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
//         return;
//     }
//
//     state.set('balance', newBalance);
//     updateText();
//
//     // emit spinFinished event when game finished
//     isGameRunning = true;
//
//     // disable panel buttons
//     disablePanelButton();
//     disableAutoSpinButton();
//
//     spinFn().then(() => {
//         panelContainer.emit('spinFinished');
//         isGameRunning = false;
//         enablePanelButton();
//         enableAutoSpinButton();
//         spinButton.texture = loaded.spinButton.texture;
//     });
//
//     panelContainer.emit('spinButtonClicked');
// }

// function onTurboButtonClicked() {
//     canWinAnimRun = false;
//     speedUp = !speedUp;
//     panelContainer.emit('turboButtonClicked', speedUp);
//     if (isMobile()) {
//         if (speedUp) {
//             turboSymbol.texture = loaded.turboSymbolActive.texture;
//         }
//         else {
//             turboSymbol.texture = loaded.turboSymbol.texture;
//         }
//     }
// }

// function enablePanelButton() {
//     spinButton.interactive = true;
//     spinButton.texture = loaded.spinButton.texture;
//
//     spin100Button.interactive = true;
//     spin100Button.texture = loaded.spin100Button.texture;
//
//     turboButton.interactive = true;
//     turboButton.alpha = 1;
//
//     betMaxButton.interactive = true;
//     betMaxButton.alpha = 1;
//
//
//     if (isMobile()) {
//         betMaxSymbol.alpha = 1;
//         turboSymbol.alpha = 1;
//         // menuButton.interactive = true;
//         // menuButton.alpha = 1;
//
//         totalBetButton.interactive = true;
//         totalBetButton.alpha = 1;
//
//         spinArea.interactive = true;
//     }
//     else {
//         betMaxLabel.alpha = 1;
//         // settingsButton.alpha = 1;
//         // settingsButton.interactive = true;
//
//         // infoMenuButton.interactive = true;
//         // infoMenuButton.buttonMode = true;
//         // infoMenuButton.alpha = 1;
//
//         if (!fixMultiBetLine) {
//             betLineDecreaseButton.interactive = true;
//             betLineDecreaseButton.alpha = 1;
//             betLineIncreaseButton.interactive = true;
//             betLineIncreaseButton.alpha = 1;
//         }
//         betLineText.alpha = 1;
//
//         betAmountIncreaseButton.interactive = true;
//         betAmountIncreaseButton.alpha = 1;
//         betAmountText.alpha = 1;
//         betAmountDecreaseButton.interactive = true;
//         betAmountDecreaseButton.alpha = 1;
//
//
//         turboLabel.alpha = 1;
//     }
// }
//
// function enableAutoSpinButton() {
//     autoSpinButton.interactive = true;
//     autoSpinButton.texture = loaded.autoSpinButton.texture;
// }
//
// function disablePanelButton() {
//     spinButton.interactive = false;
//     spinButton.texture = loaded.spinDownButton.texture;
//
//     spin100Button.interactive = false;
//     spin100Button.texture = loaded.spin100DownButton.texture;
//
//     turboButton.interactive = false;
//     turboButton.alpha = 0.5;
//
//     betMaxButton.interactive = false;
//     betMaxButton.alpha = 0.5;
//
//
//     if (isMobile()) {
//         betMaxSymbol.alpha = 0.5;
//         turboSymbol.alpha = 0.5;
//         totalBetButton.interactive = false;
//         totalBetButton.alpha = 0.5;
//         spinArea.interactive = false;
//         if (betContainer.y <= 920) {
//             TweenMax.to(betContainer, 0.5, {y: 1280});
//             // TweenMax.to(totalBetArrow, 0.5, {rotation: 0});
//         }
//
//         // menuButton.interactive = false;
//         // menuButton.alpha = 0.5;
//         // if (menuContainer.y >= 0) {
//         //     TweenMax.to(menuContainer, 0.5, {y: -600});
//         // }
//     }
//     else {
//         betMaxLabel.alpha = 0.5;
//
//         betAmountIncreaseButton.interactive = false;
//         betAmountDecreaseButton.interactive = false;
//         betLineDecreaseButton.interactive = false;
//         betLineIncreaseButton.interactive = false;
//
//         betAmountText.alpha = 0.5;
//         betLineText.alpha = 0.5;
//         betAmountIncreaseButton.alpha = 0.5;
//         betAmountDecreaseButton.alpha = 0.5;
//         betLineDecreaseButton.alpha = 0.5;
//         betLineIncreaseButton.alpha = 0.5;
//
//         // settingsButton.interactive = false;
//         // settingsButton.alpha = 0.5;
//
//         // Close settings container
//         TweenMax.to(settingsContainer, 1, {y: 645});
//         closePopUpSprite.interactive = false;
//
//         turboLabel.alpha = 0.5;
//
//         // infoMenuButton.interactive = false;
//         // infoMenuButton.buttonMode = false;
//         // infoMenuButton.alpha = 0.5;
//     }
// }
//
// function disableAutoSpinButton() {
//     autoSpinButton.interactive = false;
//     autoSpinButton.texture = loaded.autoSpinDownButton.texture;
// }

// function setWalletInDisplay(amount) {
// each(panelDisplayContainer.children, (element, index) => {
//     if (panelDisplayContainer.children[index].children[0].text === loaded.languageFile.data.balance) {
//         element.children[1].text = state.get('currency') + ' ' + utils.showAsCurrency(parseFloat(amount));
//     }
// });
// }

// function start100Spin(data) {
// terminate if newBalance less than 0;
// if (state.get('balance') < 0) {
//     // ui.flash(walletText, '0xff0000', 5);
//     ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
//     return;
// }
//
// // 100 Spin Spine
// let spin100Spine = new PIXI.spine.Spine(loaded.spin100Effect_json.spineData);
// spin100Spine.name = '100 Spin Spine';
// spin100Spine.state.setAnimation(0, 'animtion0', false);
// spin100Spine.autoUpdate = true;
// spin100Spine.x = isMobile() ? 360 : 640;
// spin100Spine.y = isMobile() ? 635 : 420;
// spin100Spine.y += spin100YOffset;
// app.addComponent(spin100Spine);
//
// sound.play('100SpinSound');
// const runningNumberContainerOptions = {
//     // scaling and offset option available to adjust but currently use default
// };
// let runningNumber = {
//     val: 0,
//     dummyNumber: 0
// };
//
// let runningNumberContainer = new PIXI.Container();
// TweenMax.to(runningNumber, 4.5 - spin100YAddDelay, {
//     dummyNumber: 200,
//     delay: spin100YAddDelay,
//     // Starts bonus running numbers
//     onStart: () => {
//         runningNumberContainer.y = app.getResolutionY() / 2;
//
//         TweenMax.to(runningNumber, 2.5, {
//             val: data.total_win,
//             ease: Power4.easeInOut,
//             onUpdate: function () {
//                 if (runningNumberContainer)
//                     app.stage.removeChild(runningNumberContainer);
//
//                 let updatedString = utils.showAsCurrency(runningNumber.val);
//                 updatedString = updatedString.toString();
//
//                 runningNumberContainer = ui.createSpriteNumbers(runningNumberContainerOptions, spriteNumberTexture, updatedString, spin100NumberBacking);
//                 runningNumberContainer.x = app.getResolutionX() / 2 - 10 - runningNumberContainer.getBounds().width / 2;
//                 runningNumberContainer.y = isMobile() ? app.getResolutionY() / 2 - 0 : app.getResolutionY() / 2 + 60;
//                 runningNumberContainer.y += spin100YTextOffset;
//                 app.addComponent(runningNumberContainer);
//             },
//         });
//     },
//
//     // Updates y value of bonus title and running numbers based on spine.y
//     onUpdate: () => {
//         if (!runningNumberContainer)
//             return;
//     },
//
//     onComplete: function () {
//         let panelMove = [
//             {amount: utils.showAsCurrency(data.total_win), text: loaded.languageFile.data.win},
//         ]
//         runningNumberContainer.destroy();
//
//         // Removes the spin from stage after 1 second
//         setTimeout(() => {
//             app.stage.removeChild(spin100Spine);
//         }, 1000);
//
//         isGameRunning = false;
//
//         if (data.total_win > 0) {
//             state.set('balance', data.balance);
//             // ui.flash(walletText, '0x00FF00', 5);
//             updateText();
//         }
//
//         // close black backing
//         backing.close();
//
//         // enable back button
//         enablePanelButton();
//         enableAutoSpinButton();
//     },
// });
// }

// function exitConfirmation() {
//     var msg = loaded.languageFile.data.exit_confirmation;
//     return msg;
// }
//
// function exitGame() {
//     let answer = confirm(loaded.languageFile.data.exit_confirmation);
//
//     if (answer) {
//         window.close();
//
//         // note: as now we open the game on new window from gamegeon.com, we can close the window instead of
//         // redirect player back to landing page.
//         // window.location = environment.gamegeonUrl;
//     }
// }
//
//
// function getAutoSpinRunning() {
//     return autoSpinRunning;
// }
//
// function playWinEffect(type, startValue, endValue, frontObjectName = 'Info Container') {
//     return new Promise((resolve) => {
//         backing.setAndOpen('Particles Coin Container');
//         let value = {val: startValue};
//         let runningNumber;
//
//         let infoContainerIndex = utils.getArrayIndexNumByName(app.stage.children, frontObjectName);
//         let winEffectSpine = type === 'mega' ? new PIXI.spine.Spine(loaded.winMega.spineData) : new PIXI.spine.Spine(loaded.winBig.spineData);
//         winEffectSpine.autoUpdate = true;
//         winEffectSpine.name = 'win effect spine';
//         winEffectSpine.x = app.getResolutionX() / 2;
//         winEffectSpine.y = app.getResolutionY() / 2;
//         winEffectSpine.state.setAnimation(0, 'animtion0', false);
//         winEffectSpine.state.addListener({
//             complete: () => {
//                 TweenMax.to(runningNumber, 1, {y: runningNumber.y - 200,
//                     onComplete: () => {
//                         setTimeout(() => {
//                             winEffectSpine.destroy();
//                             runningNumber.destroy();
//                             backing.close();
//                             resolve(0);
//                         }, 100);
//                     }
//                 })
//             }
//         })
//         app.stage.addChildAt(winEffectSpine, infoContainerIndex - 1);
//
//         let winEffectIndex = utils.getArrayIndexNumByName(app.stage.children, 'win effect spine');
//         TweenMax.to(value, 1.4, {val: endValue,
//             onUpdate: () => {
//                 if(runningNumber) {
//                     runningNumber.destroy();
//                 }
//
//                 runningNumber = ui.createSpriteNumbers({}, getSpriteNumberTexture(), utils.showAsCurrency(value.val));
//                 runningNumber.x = app.getResolutionX() / 2 - runningNumber.getBounds().width / 2;
//                 runningNumber.y = app.isMobile() ? app.getResolutionY() * 0.6 : app.getResolutionY() * 0.62;
//                 app.stage.addChildAt(runningNumber, winEffectIndex + 1);
//             }
//         })
//     });
// }
//
// function getTotalBetAmount() {
//     return state.get('betAmount') * state.get('betLine');
// }

export default {
    init,
    changeWeapon,
    popUp,
    updateAttack,
    getWeapon,
    displayWinAmountWithSprites,
    getSpriteNumberTexture
    // updateText,
    // displayWinAmountWithSprites,
    // getAutoSpinRunning,
    // runPreEvents,
    // enablePanelButton,
    // enableAutoSpinButton,
    // disablePanelButton,
    // disableAutoSpinButton,
    // playWinEffect,
    // getTotalBetAmount,
}
