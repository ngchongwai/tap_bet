export default {
    environmentName: 'localhost',
    apiPrefix: 'http://localhost:8000/api',
    socketUrl: 'http://localhost:3000',
    // apiPrefix: 'http://192.168.2.65:8000',
    transactionHistory: 'https://file.staging.majagames.com/gamegeon-bet-history',
    gameName: 'tap-bet',
    gamegeonUrl: 'https://staging.majagames.com',
    debug: true,
    allowCheatMode: true,
    googleAnalyticCode: 'UA-124821115-1',
}
