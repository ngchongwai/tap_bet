export default {
    environmentName: 'staging',
    apiPrefix: 'https://api.staging.majagames.com/api',
    socketUrl: 'https://api.staging.majagames.com/socket',
    // apiPrefix: 'http://192.168.2.62:8000/api',
    transactionHistory: 'https://file.staging.majagames.com/gamegeon-bet-history',
    gameName: 'tap-bet',
    gamegeonUrl: 'https://staging.majagames.com',
    debug: true,
    allowCheatMode: true,
    googleAnalyticCode: 'UA-124821115-1',
}
