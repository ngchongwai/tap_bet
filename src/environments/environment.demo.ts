export default {
    environmentName: 'demo',
    apiPrefix: 'https://api.demo.majagames.com/api',
    socketUrl: 'https://api.demo.majagames.com/socket',
    transactionHistory: 'https://file.demo.majagames.com/gamegeon-bet-history',
    gameName: 'tap-bet',
    gamegeonUrl: 'https://majagames.com',
    debug: true,
    allowCheatMode: false,
    googleAnalyticCode: 'UA-124821115-2',
}
