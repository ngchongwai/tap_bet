export default {
    environmentName: 'production',
    apiPrefix: 'https://api.majagames.com/api',
    socketUrl: 'https://api.majagames.com/socket',
    transactionHistory: 'https://file.majagames.com/gamegeon-bet-history',
    gameName: 'tap-bet',
    gamegeonUrl: 'https://majagames.com',
    debug: false,
    allowCheatMode: false,
    googleAnalyticCode: 'UA-124821115-3',
}
